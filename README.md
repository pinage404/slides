# Slides

The slides are only a support for the presentation

You will miss the other part of what i am presenting

The hosted slides are my backup media

* [Mob 🇬🇧](https://gitlab.com/pinage404/slides/-/tree/main/mob/)
  * [Mob 101 🇬🇧](https://pinage404.gitlab.io/slides/mob_basics/)
  * [Facilitator tips 🇬🇧](https://pinage404.gitlab.io/slides/mob_facilitator_tips/)
  * Alternative modes
    * [Fishbowl 🇬🇧](https://pinage404.gitlab.io/slides/mob_fishbowl/)
    * [Ping Pong TDD 🇬🇧](https://pinage404.gitlab.io/slides/mob_ping_pong_tdd/)
  * [Pair Programming](https://gitlab.com/pinage404/slides/-/blob/main/mob/exercices.md#pair-programming)
* [CI / CD 🇫🇷](https://pinage404.gitlab.io/slides/ci_cd)
  * [Démo 🇫🇷](https://gitlab.com/pinage404/slides/-/tree/main/ci_cd)
* [Tests 🇫🇷](https://pinage404.gitlab.io/slides/tests/)
* [Reproductible Dev Env 🇫🇷](https://pinage404.gitlab.io/slides/reproductible_dev_env/)
* [Historique de l'écosystème JavaScript 🇫🇷](https://pinage404.gitlab.io/slides/javascript_ecosystem/)
* [CLI FTW 🇫🇷](https://pinage404.gitlab.io/slides/cli_ftw/)
  * [Demo](https://gitlab.com/pinage404/slides/-/tree/main/cli_ftw)
* [Rust 🇫🇷](https://pinage404.gitlab.io/slides/rust/)
* [`git` 🇫🇷](https://gitlab.com/pinage404/slides/-/tree/main/git)
  * [`git` général 🇫🇷](https://pinage404.gitlab.io/slides/git_general/)
  * [`git` commitish 🇫🇷](https://pinage404.gitlab.io/slides/git_commitish/)
  * [`git merge` & `git rebase` 🇫🇷](https://pinage404.gitlab.io/slides/git_merge_rebase/)
  * [`git` conflit 🇫🇷](https://pinage404.gitlab.io/slides/git_conflit/)
  * [`git remote` 🇫🇷](https://pinage404.gitlab.io/slides/git_remote/)
* Programmation Fonctionelle
  * [C'est quoi un monoïde ? 🇫🇷](https://pinage404.gitlab.io/slides/functional_programming_monoid/)
* [Toki Pona Quizz 🇫🇷 & 🇬🇧](https://pinage404.gitlab.io/slides/toki_pona_quizz/)

For the licence right, please [read the note](https://pinage404.is-a.dev/#license)

[![Licence Creative Commons BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)](https://creativecommons.org/licenses/by-nc-sa/4.0/)
by
[![pinage404](https://s.gravatar.com/avatar/0a7d96df27d5d020cb0d03340e734180?s=40)](https://wheretofind.me/@pinage404)
on
[![GitLab](https://img.shields.io/gitlab/stars/pinage404/slides?style=social)](https://gitlab.com/pinage404/slides)

<link rel="stylesheet" href="https://pinage404.gitlab.io/style.css" />
