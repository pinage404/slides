# Audience Transformation Roadmap

From [The 3 Magic Ingredients of Amazing Presentations | Phil WAKNELL | TEDxSaclay](https://www.youtube.com/watch?v=yoD8RMq2OkU)

| What they | Before | Transformation | After |
|:----------|:-------|:---------------|:------|
| Know      |        |                |       |
| Believe   |        |                |       |
| Feel      |        |                |       |
| Do        |        |                |       |

Fill :

1. Before ⬇️
1. After ⬆️
1. Transformation ⬇️
