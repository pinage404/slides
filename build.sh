#!/usr/bin/env sh
set -o errexit -o nounset

ROOT_FOLDER="$(pwd)"
SLIDES_FOLDER="$ROOT_FOLDER/slides"
BUILD_FOLDER="$ROOT_FOLDER/dist"
OUTPUT_FOLDER="$ROOT_FOLDER/public"
if [ -n "${CI_PROJECT_NAME+empty}" ]; then
    BASE_URL="/$CI_PROJECT_NAME"
else
    BASE_URL=""
fi

clean_output_folder() {
    rm -rf "$OUTPUT_FOLDER"
    mkdir "$OUTPUT_FOLDER"
}

build_slides_once() {
    FOLDER="$1"

    pnpm run build \
        --base "$BASE_URL/$FOLDER/" \
        --out "$BUILD_FOLDER/$FOLDER/" \
        "$SLIDES_FOLDER/$FOLDER/slides.md"
}

build_slides_all() {
    for FOLDER in "$SLIDES_FOLDER"/*/; do
        build_slides_once "$(basename "$FOLDER")"
    done
}

assemble_redirections() {
    cat "$BUILD_FOLDER"/*/_redirects >>"$OUTPUT_FOLDER/_redirects"
    rm "$BUILD_FOLDER"/*/_redirects
}

move_built_to_output() {
    mv "$BUILD_FOLDER"/* "$OUTPUT_FOLDER"
}

main() {
    clean_output_folder

    build_slides_all

    assemble_redirections

    move_built_to_output
}

main
