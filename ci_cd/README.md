# Démo GitLab CI

---

1. expliquer le besoin / exercice

   > un programme qui dit "bonjour" aux gens

1. setup l'env en local

   ```sh
   echo "pytest~=6.0" >> requirements.txt
   pip3 install --requirement requirements.txt
   ```

1. écrire le programme

   `hello.py`

   ```python
   def hello(name):
       return 'Hello world'

   def test_hello_world():
       assert hello() == 'Hello world'
   ```

1. tester en local

   ```sh
   pytest hello.py
   ```

1. créer le repo local

   ```sh
   git init
   git add .
   git commit
   ```

1. créer le repo [sur GitLab](https://gitlab.com/projects/new) et ajouter la remote

   ```sh
   git remote add origin git@gitlab.com:USER/REPO.git
   ```

1. décrire la pipeline

   `.gitlab-ci.yml`

   ```yaml
   test:
       image: python:3-alpine
       script:
           - pip3 install --requirement requirements.txt
           - pytest hello.py
   ```

1. lancer la pipeline `git push origin main`
1. montrer que la pipeline fonctionne
1. expliquer `job` > `script` > `command`
1. compléter la pipeline avec le déploiement en utilisant `GitLab Pages`:
   serveur de fichiers statiques gratuit

   ```sh
   echo '<a href="hello.py">hello</a>' >> index.html
   ```

   `.gitlab-ci.yml`

   ```yaml
   test:
       image: python:3-alpine
       script:
           - pip3 install --requirement requirements.txt
           - pytest hello.py

   pages:
       script:
           - mkdir public
           - mv index.html hello.py public
       artifacts:
           paths:
               - public/
           expire_in: 1 week
   ```

1. aller sur la page
    `https://USER.gitlab.io/REPO/index.html`
1. télécharger le fichier
1. ajouter un test qui fail

   `hello.py`

   ```python
   def hello():
       return 'Hello world'

   def test_hello_world():
       assert hello() == 'Hello world'

   def test_hello_toto():
       assert hello('toto') == 'Hello toto'
   ```

1. montrer que la CI fail
1. montrer que le fichier téléchargé est mis à jour malgrès le problème
1. corriger le test

   `hello.py`

   ```python
   def hello(name='world'):
       return f'Hello {name}'

   def test_hello_world():
       assert hello() == 'Hello world'

   def test_hello_toto():
       assert hello('toto') == 'Hello toto'
   ```

1. montrer que le fichier téléchargé est mis à jour
1. ajouter différents `stages`

   `.gitlab-ci.yml`

   ```yaml
   stages:
       - test
       - deploy

   test:
       stage: test
       image: python:3-alpine
       script:
           - pip3 install --requirement requirements.txt
           - pytest hello.py

   pages:
       stage: deploy
       script:
           - mkdir public
           - mv index.html hello.py public
       artifacts:
           paths:
               - public/
           expire_in: 1 week
   ```

1. montrer la pipeline multi-`stages`
1. expliquer `pipeline` > `stage` > `job` > `script` > `command`
1. ajouter un test qui fail
1. montrer que la pipeline fail
1. montrer que le fichier téléchargé est resté sur la version précédente
1. corriger le test

[GitLab YAML Reference](https://docs.gitlab.com/ee/ci/yaml/README.html)
