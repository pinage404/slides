def hello(name='world'):
    return f'Hello {name}'

def test_hello_world():
    assert hello() == 'Hello world'

def test_hello_toto():
    assert hello('toto') == 'Hello toto'
