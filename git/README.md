# Git

* [`git` général](https://pinage404.gitlab.io/slides/git_general/)
* [`git` commitish](https://pinage404.gitlab.io/slides/git_commitish/)
* [`git merge` & `git rebase`](https://pinage404.gitlab.io/slides/git_merge_rebase/)
* [`git` conflit](https://pinage404.gitlab.io/slides/git_conflit/)
* [`git remote`](https://pinage404.gitlab.io/slides/git_remote/)
* Exercice
  * [GPG / PGP](https://gitlab.com/pinage404/slides/-/blob/main/git/exercice.md)
  * [`git merge` & `git rebase`](https://gitlab.com/pinage404/slides/-/blob/main/git/git_merge_rebase.md)
