# GPG : Git Paper Graph

PGP : Play Git Paper

## Nombre de personnes par groupe

Impair

* 3 minimum
* 5 idéal
* 7 maximum

## Logistique

* 1 par personne
  * 📄 Papier
    * A4 minimum
  * 🖊️ Stylo

## Plis

|       |         |         |             |         |
| :---- | :------ | :------ | :---------- | :------ |
| Recto | 🚫       | 1 Graph | 2 Commandes | 3 Graph |
| Verso | 5 Graph | 🚫       | 4 Commandes | 🚫       |

* ✅ Recto ↔️ Verso
* ❎ Recto ↕️ Verso

## Phases

1. Préparation
   1. Pliages
   1. Numérotations
   1. Démo pliage
   1. Explications
1. Jeu
   * 5~7 min / tour
   * 5 tours
   * Time keeper
   * Numéro de tour
   * 🚫👣
1. Comparaison
1. Rétro
1. Liste de mécompréhensions
1. Pause
1. Démo / Questions / Réponses / Théorie / Pratique

## Convention

* Écriture
  * Majuscule
  * ~~git~~
  * cherry pick `A` => `A'`
* Interprétation
  * Non dit => par défaut
  * Impossible => ignore
  * Méconnaissance => imaginer

<!-- markdownlint-disable -->
<p class="legal">
    <a rel="license" href="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">
        <img alt="Licence Creative Commons BY-NC-SA" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" />
    </a>
    by
    <a rel="author external" href="https://wheretofind.me/@pinage404">
        <img alt="pinage404" src="https://s.gravatar.com/avatar/0a7d96df27d5d020cb0d03340e734180?s=40" />
    </a>
    on
    <a rel="alternate" href="https://gitlab.com/pinage404/initiation_git">
        <img alt="GitLab" src="https://img.shields.io/gitlab/stars/pinage404/initiation_git?style=social" />
    </a>
</p>
<style>
.legal img {
    margin: 0;
    vertical-align: middle;
}
</style>
