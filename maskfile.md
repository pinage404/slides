# Commands

## run (SLIDE_PATH)

```sh
pnpm start "$SLIDE_PATH"
```

## update

```bash
set -o errexit -o nounset -o pipefail -o errtrace

nix flake update
direnv exec . \
    devbox update
direnv exec . \
    corepack use pnpm@latest
direnv exec . \
    pnpm update --latest
```

---

<!-- markdownlint-disable-next-line MD039 MD045 -->
This folder has been setup from the [`nix-sandboxes`'s template ![](https://img.shields.io/gitlab/stars/pinage404/nix-sandboxes?style=social)](https://gitlab.com/pinage404/nix-sandboxes)
