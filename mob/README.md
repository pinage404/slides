# Mob Programming

## Modules

* [Mob 101](https://pinage404.gitlab.io/slides/mob_basics/)
  * Format : Theory + Practice
  * Duration : ~2h
  * Objective : Align everyone with the mob basics
* [Facilitator tips](https://pinage404.gitlab.io/slides/mob_facilitator_tips/)
  * Format : Theory + Practice
  * Duration : ~2h minimum
  * Objective : Give tips for facilitating a group
* Alternative modes
  * [Fishbowl](https://pinage404.gitlab.io/slides/mob_fishbowl/)
    * Format : Theory + Practice
    * Duration : 1h30~2h
    * Objective : Discover a method of organization,
      very fun but only feasible in physics
    * Constraint : on-site
  * [Ping Pong TDD](https://pinage404.gitlab.io/slides/mob_ping_pong_tdd/)
    * Format : Theory + Practice
    * Duration : ~2h
    * Objective : Discover a method of organization based on tests
* [Pair Programming](exercices.md#pair-programming)
  * Format : Practice
  * Duration : ~2h
  * Objective : Illustrate what is important in Pair Programming
    with constrained exercises
  * Constraint : on-site

## Dependencies

```mermaid
flowchart LR
    101[Mob 101] --> Facilitator[Facilitator tips]
    101[Mob 101] --> fishbowl[Fishbowl]
    ping_ping[Ping Pong TDD]
    pair[Pair Programming]
```
