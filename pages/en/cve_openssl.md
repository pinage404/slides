---
layout: section
---

2022-11-01 📆

<v-click>

### `CVE-2022-3602`

[`CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H`](https://github.com/advisories/GHSA-8rwr-x37p-mx23)

</v-click>

---
class: no-ligatures
---

```diff
         n = n + i / (written_out + 1);
         i %= (written_out + 1);

-        if (written_out > max_out)
+        if (written_out >= max_out)
             return 0;

         memmove(pDecoded + i + 1, pDecoded + i,
                 (written_out - i) * sizeof *pDecoded);
```

<!--
OpenSSL

~~critical~~ _high_ vulnerability

* [Commit](https://github.com/openssl/openssl/commit/fe3b639dc19b325846f4f6801f2f4604f56e3de3)
* [Changelog](https://github.com/openssl/openssl/blob/openssl-3.2.1/CHANGES.md#changes-between-306-and-307-1-nov-2022)
-->
