---
layout: quote
---

<!-- markdownlint-disable MD013 -->

> Estimating tasks will slow you down.
> Don't do it.
> We gave it up over 10 years ago.
>
> […]
>
> Best teams have small stories and do no tasking.
> They move to acceptance test driven development.
>
> […]
>
> [Scrum is intended] to enhance team performance,
> not to have them suck estimating tasks in hours.
> -- [<v-click>Jeff Sutherland</v-click> 2018][quote] <v-click>Inventor and Co-Creator of Scrum</v-click>

[quote]: https://www.quora.com/What-are-the-techniques-set-by-the-Scrum-guidelines-for-a-task-estimation-in-sprint-planning-Are-there-any-limitations-to-these-techniques/answer/Jeff-Sutherland-10
