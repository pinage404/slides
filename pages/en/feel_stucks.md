### Feel stucks ?

<v-click>

So it's often a good idea to :

</v-click>

<v-clicks>

1. **take a break** ⏸️
1. then _slow down_ ⏳
1. with _smaller steps_ 🎯
1. and maybe _rollback_ 🔙
1. and maybe _make a plan_ 🗺️

</v-clicks>
