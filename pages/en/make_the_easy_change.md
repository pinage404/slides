---
layout: quote
---

> for each desired change,
>
> make the change easy
> <span v-mark.circle>
> **(warning: this may be hard)**,
> </span>
>
> then make the easy change
>
> -- [Kent Beck 2012](https://twitter.com/KentBeck/status/250733358307500032)
