---
layout: quote
---

> _Compromise_ is essential
> because
> **there are no ideal solutions**,
> _only trade-offs_.
>
> -- Thomas Sowell 1987
> [_A Conflict of Visions_ 📖](https://en.wikipedia.org/wiki/A_Conflict_of_Visions)
