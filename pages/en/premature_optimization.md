---
layout: quote
---

> Premature optimization is the root of all evil
>
> -- Donald Knuth 1974
> [_Computer Programming as an Art_ 📖](https://en.wikiquote.org/wiki/Donald_Knuth#Computer_Programming_as_an_Art_(1974))
