---
layout: quote
---

### 🗣️🧑‍💻️

Strong Style

> For an idea to go from your head into the computer
> it MUST go through someone else's hands
>
> -- [Llewellyn Falco 2014](https://llewellynfalco.blogspot.com/2014/06/llewellyns-strong-style-pairing.html)
