```mermaid
flowchart TB
    subgraph refactor[Refactor]
        direction LR

        rewrite_code[Rewrite code without changing the behavior]
        ==> run_tests{{Run tests}}
        -.->|Pass. Another things to refactor ?| rewrite_code

        run_tests
        -->|Fail. Change something else| rewrite_code
    end

    green([Green]) ==> rewrite_code

    run_tests ==>|Pass| finish([Finish])
    run_tests -.->|Pass. Another feature to add ?| red([Red])

    classDef red_phase font-weight:bold,color:black,fill:coral;
    class red red_phase

    classDef green_phase font-weight:bold,color:black,fill:#1cba1c;
    class green green_phase

    classDef refactor_phase font-weight:bold,color:black,fill:#668cff;
    class refactor refactor_phase

    classDef fix_layout stroke:white,fill:transparent;
    class fix_layout fix_layout
```
