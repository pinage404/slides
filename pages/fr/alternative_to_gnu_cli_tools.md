---
layout: section
---

<!-- markdownlint-disable MD003 MD022 MD041 -->

## GNU core utilities

### Alternatives Modernes

<!--
Linux / Mac coreutils
-->

---

[eza](https://eza.rocks/) 📂 lister le contenu d'un dossier

comme GNU `ls` + GNU `tree` mais :

* `+` facile
  * options explicites
* Git integration
* couleurs

---

[eza](https://eza.rocks/) 📂 lister le contenu d'un dossier

![Démo](/eza.gif)

---

[fd](https://github.com/sharkdp/fd) 🔎 chercher _des fichiers_

comme GNU `find` mais :

* `+` facile
  * options explicites
  * regexp
* ⚡ rapide
  * respecte le `.gitignore`

---
layout: image
image: https://github.com/sharkdp/fd/raw/68fe31da3f5da5d8d5b997d8919dc97e6eafead5/doc/screencast.svg
background-size: contain
---

---

[ripgrep](https://github.com/BurntSushi/ripgrep) 🔍 chercher _**dans** des fichiers_

comme GNU `grep` mais :

* `+` facile
  * options explicites
  * regexp
* ⚡ rapide
  * respecte le `.gitignore`

---

[ripgrep](https://github.com/BurntSushi/ripgrep) 🔍 chercher _**dans** des fichiers_

![Démo](https://burntsushi.net/stuff/ripgrep1.png)

---

[bat](https://github.com/sharkdp/bat) 📄 afficher un fichier

comme GNU `cat` + `nl` mais :

* syntax highlight
* Git integration

---

[bat](https://github.com/sharkdp/bat) 📄 afficher un fichier

![Démo](/bat.png)

---

[mdcat](https://github.com/swsnr/mdcat) 📄 afficher un fichier _Markdown_

comme GNU `cat` mais :

* syntax highlight
* render Markdown

![démo](https://github.com/swsnr/mdcat/raw/d14825c45bf2828280f9a90ea3f9e0f428a82274/screenshots/side-by-side.png)

---

[ncdu](https://dev.yorhel.nl/ncdu) 📊 trouver les dossiers lourds

comme GNU `du` mais :

* graphique
* tri
  * critères
* suppresion de fichiers
* raccourcis clavier
* couleurs

---

[ncdu](https://dev.yorhel.nl/ncdu) 📊 trouver les dossiers lourds

![démo](https://dev.yorhel.nl/img/ncduconfirm-2.png)

---

[watchexec](https://watchexec.github.io/) 🔄 répéter une commande

ressemble à GNU `watch` mais :

* s'éxécute dès qu'un fichier change

```sh
watchexec make
```

---

[sd](https://github.com/chmln/sd) 🔍 remplacer

ressemble à GNU `sed` mais :

* regexp
* chercher et remplacer _uniquement_

```sh
cat fichier | sd before after
sd before after fichier
```

---

[htop](https://htop.dev/) gérer les processus

ressemble à GNU `top` mais :

* facile
  * raccourcis claviers _affichés_
* couleurs
* cliquable
* graph

---
layout: image
image: 'https://htop.dev/images/htop_graph.gif'
background-size: contain
---

---

[btop](https://github.com/aristocratos/btop) 📊 graphique système

ressemble à GNU `top` mais :

* **graph**
* facile
  * raccourcis claviers _affichés_
* couleurs
* cliquable

---

[btop](https://github.com/aristocratos/btop) 📊 graphique système

![Démo](https://github.com/aristocratos/btop/raw/d1680735d9329884c2d46f545a36df32ca5e40cf/Img/normal.png)

---

[ctop](https://ctop.sh/) 📊 trouver les containers gourmands

ressemble à GNU `top` mais :

* pour les containers

![Démo](https://ctop.sh/img/screencap.gif)
