---
layout: section
---

## ❤️ + ❤️ = 😍

---
layout: full
---

```mermaid
flowchart TB
    nix_files[Nix files]

    DirEnv
    -->|load| DevBox

    subgraph DevBox
        direction LR

        DevBox_internal[DevBox]
        -->|generate| nix_files
        -->|declare dependencies| Nix
        -->|install dependencies| Nix
    end
```

---

```sh
devbox generate direnv
```

<v-click>

`.envrc`

```sh
eval "$(devbox generate direnv --print-envrc)"
```

</v-click>

---

`.devbox.json`

```json
{
  "packages": [
    "nodejs@21.6",
    "ruby@2.7",
  ]
}
```

---

`.envrc`

```sh {all|1|3|5-14|16|all}
eval "$(devbox generate direnv --print-envrc)"

export NODE_OPTIONS="--max-old-space-size=4096 --openssl-legacy-provider"

# installation des dépendances quand nécessaire
if [ ! -d ./node_modules/ ]; then
  npm install
fi
if [ ! -d ./ios/Pods/ ]; then
  pushd "./ios/"
  bundle install
  bundle exec pod install --repo-update
  popd
fi

layout node # commande sans prefixer par ./node_modules/.bin/ ou npx ou yarn
```
