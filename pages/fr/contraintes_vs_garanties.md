---
layout: section
---

**Contraintes** 😒 🆚 **Garanties** 😌

🎭 🪙

<!--
2 faces d'une meme pièce
-->

---

Contrainte : s'arreter au feu rouge 🚶🚦🚗

<v-click>

Garantie : éviter un accident au carrefour 🚗💨😵🚦

</v-click>

---

Contrainte : cotiser pour le chomage 💸

<v-clicks>

Garantie `*` : éviter de se retrouver à la rue si on perd son taf 👥💰😌

`*` _si on oublie les néo-libéraux_

</v-clicks>
