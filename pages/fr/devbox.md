---
---

### Version spécifique

````md magic-move
```nix {all|12,13}
{
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages."${system}";
      in
      {
        devShell = pkgs.mkShell {
          packages = [
            pkgs.nodejs
            pkgs.ruby
          ];
        };
      });
}
```

```nix {12,13}
{
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages."${system}";
      in
      {
        devShell = pkgs.mkShell {
          packages = [
            pkgs.nodejs # 20.11.1
            pkgs.ruby # 3.1.4
          ];
        };
      });
}
```
````

<!--
je voudrais :

* nodejs
  * 20.11.1 -> 21.6
* ruby
  * 3.1.4 -> 2.7
-->

---
layout: section
---

## Problème 3

---

|                       | Me 🧑‍💻 |
| --------------------: | :----: |
| Nix (package manager) |  ❤️   |
|            Nix (lang) |  😠   |

---

### [DevBox](https://www.jetify.com/devbox/)

<v-clicks>

Outils permettant de gérer son env

en utilisant Nix (package manager)

en générant des fichiers Nix (lang)

sans devoir écrire du Nix (lang)

</v-clicks>

---
layout: full
---

```mermaid
flowchart LR
    nix_files[Nix files]

    DevBox
    -->|generate| nix_files
    -->|declare dependencies| Nix
    -->|install dependencies| Nix
```

---
src: ../en/nix_vs_devbox.md
---

---

```sh
devbox add nodejs
```

<div v-click>

`devbox.json`

```json
{
  "packages": [
    "nodejs@latest",
  ]
}
```

</div>

---
layout: full
style: "overflow: auto;"
---

`devbox.lock`

```json {all,6}
{
  "lockfile_version": "1",
  "packages": {
    "nodejs@latest": {
      "last_modified": "2024-02-15T12:53:33Z",
      "resolved": "github:NixOS/nixpkgs/085589047343aad800c4d305cf7b98e8a3d51ae2#nodejs_21",
      "source": "devbox-search",
      "version": "21.6.2",
      "systems": {
        "aarch64-darwin": {
          "store_path": "/nix/store/g1v8xrnj54xyvn6gf73mnw9pdwrdzmdi-nodejs-21.6.2"
        },
        "aarch64-linux": {
          "store_path": "/nix/store/7ywds3cnww6ahc45g28c3c2rnf2aky2s-nodejs-21.6.2"
        },
        "x86_64-darwin": {
          "store_path": "/nix/store/scbarnsw7m2g4j2m6rp5c82wyv1fzg8l-nodejs-21.6.2"
        },
        "x86_64-linux": {
          "store_path": "/nix/store/6cip7ljx563rlyfnzwkfzf6gxpy1lid0-nodejs-21.6.2"
        }
      }
    }
  }
}
```

---

```sh
devbox add nodejs@21.6
```

<div v-click>

`devbox.json`

```json
{
  "packages": [
    "nodejs@21.6",
  ]
}
```

</div>
