---
layout: section
---

<!-- markdownlint-disable MD041 -->

## DevOps

<!-- markdownlint-disable-next-line MD013 -->
<v-click at="1">❌</v-click> <span v-mark.crossed="{ at: 1 }">Role</span> <v-click at="1">⚠️</v-click>

<v-clicks>

DevOps ~ Agile

Mindset / grands principes

1. Communication
2. Travail collaboratif
3. Outil

[Agile Manifesto](https://agilemanifesto.org)

[12 Factor App](https://12factor.net)

</v-clicks>
