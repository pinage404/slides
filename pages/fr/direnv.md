---
layout: section
---

## Problème 1

---

Qui a déjà eu ce type d'expérience ?

```sh
node --print "process.env.NODE_ENV === 'production'"
bash: node: command not found
```

<v-clicks>

```sh
nvm use
node --print "process.env.NODE_ENV === 'production'" # false
```

```sh
NODE_ENV="production" node --print "process.env.NODE_ENV === 'production'" # true
NODE_ENV="production" node --print "process.env.NODE_ENV === 'production'" # true
```

```sh
export NODE_ENV="production"
node --print "process.env.NODE_ENV === 'production'" # true
node --print "process.env.NODE_ENV === 'production'" # true
```

</v-clicks>

<!--
programme chargé par une autre commande ;

executer une commande avec une variable d'environnement spécifique
-->

---

### Solution

Modifier son environnement

* `~/.profile`
* `~/.bashrc`
* `~/.bash_profile`
* `~/.zshrc`
* `~/.config/fish/config.fish`
* …

---

### Problème 1.1

<v-click>

Environnement partagé pour tout les dossiers / projets / branches

</v-click>

---

### Solution

---

#### [DirEnv](https://direnv.net/)

`.envrc`

```sh
export NODE_ENV="production"
```

<!--
environnement par dossier
-->
