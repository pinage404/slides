### ⚠️

#### 🦸🏻‍♂️🧑🏻‍🦼❌

<!--
je ne suis pas Charles Xavier / Professor X : je ne suis pas télépathe

donc si vous ne dites pas que vous ne comprennez pas,
je ne peux pas le savoir

il n'y a aucune questions stupides

si vous ne dites pas que vous ne savez pas, vous n'apprendrez pas

n'hésitez pas à demander tant que vous n'avez pas compris
-->
