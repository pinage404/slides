---
layout: quote
---

> Seul, on va plus vite.
>
> Ensemble, on va plus loin.
