---
style: |
  background-color: rgb(0 0 0 / var(--un-bg-opacity));
  color: rgb(255 255 255 / var(--un-text-opacity))
---

<!-- markdownlint-disable MD001 MD024 -->

#### F.I.R.S.T. Principles 1️⃣

* **F**
* **I**
* **R**
* **S**
* **T**

<!--
faire deviner les principes
-->

---

#### F.I.R.S.T. Principles 1️⃣

* **F**ast
  * < secondes
  * lent ➞ flemme ➞ non-exécuté ➞ ignoré
* **I**
* **R**
* **S**
* **T**

---

#### F.I.R.S.T. Principles 1️⃣

* **F**ast
* **I**solated
  * pas de dépendances entre les tests
  * pas d'ordre d'exécution des tests
  * évite les problèmes en cascade
* **R**
* **S**
* **T**

---

#### F.I.R.S.T. Principles 1️⃣

* **F**ast
* **I**solated
* **R**epeatable
  * "erreur normale" / "test flaky" ➞ ignoré
    * faux négatif / vrai négatif ?
    * faux positif 😰
* **S**
* **T**

---

#### F.I.R.S.T. Principles 1️⃣

* **F**ast
* **I**solated
* **R**epeatable
* **S**elf-validating
  * `assert`ion dans chaque test
  * pas de vérification manuelle dans l'output / un fichier
    * fastidieux ➞ lent ➞ flemme ➞ non-exécuté ➞ ignoré
    * subjectif ➞ désaccord sur le résultat
* **T**

---

#### F.I.R.S.T. Principles 1️⃣

* **F**ast
* **I**solated
* **R**epeatable
* **S**elf-validating
* **T**imely
  * _code de **test**_ écrit **avant** _code de **prod**_

---

#### F.I.R.S.T. Principles 1️⃣

* **F**ast
* **I**solated
* **R**epeatable
* **S**elf-validating
* **T**imely

[_Writing your F.I.R.S.T. Unit Tests_ par Lucas Fonseca Mundim](https://dev.to/mundim/writing-your-f-i-r-s-t-unit-tests-1iop)
