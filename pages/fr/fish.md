---
layout: section
---

<!-- markdownlint-disable MD003 MD022 MD033 MD041 -->

~~Bash / ZSH~~

## [Fish](https://fishshell.com/)

![Logo](/fish_logo.png)

<!--
shell alternatif
-->

---

Autocompletion des arguments

![Démo](https://fishshell.com/assets/img/screenshots/works_out_of_the_box.png)

automatiquement généré

---

Autocompletion des options

![Démo](/20230129013215.png)

automatiquement générée pour les logiciels installés

---

Autosuggestion

![Démo](https://fishshell.com/assets/img/screenshots/autosuggestion.png)

basée sur l'historique

---

[Oh My Fish](https://github.com/oh-my-fish/oh-my-fish)

<img
  src="https://raw.githubusercontent.com/oh-my-fish/oh-my-fish/d427501b2c3003599bc09502e9d9535b5317c677/docs/logo.svg"
  style="margin: auto;"
  width="192px"
  height="192px"
  alt=""
/>

<!--
gestionnaire de paquets pour Fish
-->

---

[enlarge_your_git_alias](https://gitlab.com/pinage404/omf_pkg_enlarge_your_git_alias)

<iframe src="https://asciinema.org/a/496261/iframe" height="250"></iframe>

---
layout: default
zoom: 0.9
---

[gityaw](https://github.com/oh-my-fish/plugin-gityaw)

```sh
git clone https://gitlab.com/pinage404/pinage404-vscode-extension-packs
```

<v-clicks>

🕜

```sh
git push
```

🤦 HTTPS

```sh
gityaw
Processing remote origin...
Replaced 'https://gitlab.com/pinage404/pinage404-vscode-extension-packs' by 'git@gitlab.com:pinage404/pinage404-vscode-extension-packs.git'
git push
```

🙂 SSH 🔒

</v-clicks>

---

[sponge](https://github.com/meaningful-ooo/sponge.git)

---

[insist](https://gitlab.com/lusiadas/insist)

---

[done](https://github.com/franciscolourenco/done)

---

[autopair](https://github.com/jorgebucaran/autopair.fish)

<kbd>([{}])</kbd>

---

[sudope](https://github.com/oh-my-fish/plugin-sudope)

```sh
fdisk --list
```

<v-clicks>

```txt
fdisk: impossible d'ouvrir /dev/sda: Permission denied
fdisk: impossible d'ouvrir /dev/sdb: Permission denied
```

😠

<kbd>alt</kbd> + <kbd>S</kbd>

```sh
sudo fdisk --list
```

🙂

</v-clicks>

---

[des thèmes] pour customiser son prompt : bobthefish

![bobthefish](https://cloud.githubusercontent.com/assets/53660/18028510/f16f6b2c-6c35-11e6-8eb9-9f23ea3cce2e.gif)

[des thèmes]: https://github.com/oh-my-fish/oh-my-fish/blob/master/docs/Themes.md
