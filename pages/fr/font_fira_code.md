---
layout: section
---

## Font / Police de caractères

<!-- markdownlint-disable MD024 -->

---

[Powerline](https://github.com/powerline/fonts)

![Démo](https://www.nerdfonts.com/assets/img/nerd-fonts-powerline-extra-terminal.png)

---

[Nerd Fonts](https://www.nerdfonts.com/)

Powerline

![Démo](https://www.nerdfonts.com/assets/img/nerd-fonts-powerline-extra-terminal.png)

---

[Nerd Fonts](https://www.nerdfonts.com/)

Icones

![Démo](https://www.nerdfonts.com/assets/img/nerd-fonts-icons-in-vim.png)

---
zoom: 0.55
---

[Nerd Fonts](https://www.nerdfonts.com/)

![Démo](https://www.nerdfonts.com/assets/img/sankey-glyphs-combined-diagram.png)

---
layout: two-cols
---

![Démo](https://github.com/tonsky/FiraCode/blob/63976dd1297dfa2f3e3024889aa865cb34b16c54/extras/samples.png?raw=true)

::right::

[FiraCode](https://github.com/tonsky/FiraCode)

---
layout: two-cols
---

![Démo](https://github.com/tonsky/FiraCode/blob/63976dd1297dfa2f3e3024889aa865cb34b16c54/extras/samples2.png?raw=true)

::right::

[FiraCode](https://github.com/tonsky/FiraCode)

---
layout: two-cols
---

![Démo](https://github.com/tonsky/FiraCode/blob/63976dd1297dfa2f3e3024889aa865cb34b16c54/extras/ligatures.png?raw=true)

::right::

[FiraCode](https://github.com/tonsky/FiraCode)
