---
layout: section
---

## Des outils pratiques

---
zoom: 0.9
---

[ngrok](https://ngrok.com) 💻➡☁️ expose un serveur HTTP local sur internet

```sh
ngrok http 8080
```

<!-- markdownlint-disable MD013 -->

```txt
ngrok                                                                          (Ctrl+C to quit)
                                                                                               
Try our new native Go library: https://github.com/ngrok/ngrok-go                               
                                                                                               
Session Status                online                                                           
Session Expires               1 hour, 59 minutes                                               
Update                        update available (version 3.1.1-rc1, Ctrl-U to update)           
Terms of Service              https://ngrok.com/tos                                            
Version                       3.0.4                                                            
Region                        Europe (eu)                                                      
Latency                       14ms                                                             
Web Interface                 http://127.0.0.1:4040                                            
Forwarding                    https://dd0b-109-7-227-228.eu.ngrok.io -> http://localhost:8080  
                                                                                               
Connections                   ttl     opn     rt1     rt5     p50     p90                      
                              0       0       0.00    0.00    0.00    0.00       
```

<!-- markdownlint-enable MD013 -->

---

[ngrok](https://ngrok.com) 💻➡☁️ expose un serveur HTTP local sur internet

_pas_ open source

gratuit pour :

* 1 port
* pour un usage non commercial
* session de 2h

---

[tmate](https://tmate.io) 💻➡☁️ partage un terminal sur internet via SSH

```sh
tmate
```

```txt
Tip: if you wish to use tmate only for remote access, run: tmate -F          [0/0]
To see the following messages again, run in a tmate session: tmate show-messages
Press <q> or <ctrl-c> to continue
---------------------------------------------------------------------
Connecting to ssh.tmate.io...
Note: clear your terminal before sharing readonly access
web session read only: https://tmate.io/t/ro-3G9X8ufyGNjVXJKyZmvxVzJqP
ssh session read only: ssh ro-3G9X8ufyGNjVXJKyZmvxVzJqP@lon1.tmate.io
web session: https://tmate.io/t/6WHmpC7P8txGAY3XnSLS4dHek
ssh session: ssh 6WHmpC7P8txGAY3XnSLS4dHek@lon1.tmate.io

[0] 0:[tmux]*                              "pinage404-sabre15-nix" 00:42 31-janv.-
```

---

[jq](https://stedolan.github.io/jq/) `[{}]`➡`{}` *J*SON *Q*uery

```sh
echo '[
    {"name":"JSON", "good":true},
    {"name":"XML", "good":false}
]' | jq '[.[].name]'
```

```json
[
  "JSON",
  "XML"
]
```

---
layout: image-left
image: https://camo.githubusercontent.com/4a677ea31b3a1cbd57216c302e8e29c2a6a6a512ebed8a25491eaf8200065ad9/68747470733a2f2f692e696d6775722e636f6d2f6c55726b51424e2e706e67
background-size: contain
---

[neofetch] 🖼️ vue d'ensemble d'un système d'exploitation

[neofetch]: https://github.com/dylanaraps/neofetch

---
layout: image-right
image: https://github.com/o2sh/onefetch/raw/5bcfdc25cd37b354da1c72be6330147d1e5db1d7/assets/screenshot-2.png
background-size: contain
---

[onefetch](https://onefetch.dev/) 🖼️ vue d'ensemble d'un projet

---

[tokei] 📊 statistiques de fichiers sources par langages

[tokei]: https://github.com/XAMPPRocky/tokei

```sh
tokei
```

```txt
===============================================================================
 Language            Files        Lines         Code     Comments       Blanks
===============================================================================
 Nix                     1           47           46            0            1
-------------------------------------------------------------------------------
 Markdown                2          568            0          358          210
 |- JSON                 1            4            4            0            0
 (Total)                            572            4          358          210
===============================================================================
 Total                   3          615           46          358          211
===============================================================================
```

---
layout: image-left
image: https://user-images.githubusercontent.com/2771466/222950622-475bc6cc-7b91-47c2-86b2-5948bee4fe8e.png
background-size: contain
---

[kondo](https://github.com/tbillington/kondo) 🚮 supprime les dossiers non nécessaires

* `target`
* `node_modules`
* `.venv`

👀

---

[imagemagick](https://imagemagick.org) 🖼️ éditeur d'image

<v-click>

Convertion

```sh
magick 'toto.png' 'toto.jpg'
#            ^^^        ^^^
```

</v-click>

<v-click>

Redimentionnement

```sh
magick 'toto.jpg' -resize '120x120' 'toto.jpg'
```

</v-click>
