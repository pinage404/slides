# Niveaux

1. 😶‍🌫️ hein ‽ jamais entendu parlé
2. 😕 je me sens dépassé / je sais faire le minimum
3. 🫤 j'ai souvent besoin de me faire aider et je me sens dépassé
4. 🤔 j'arrive à mes fins sans toujours comprendre, ni être optimal
5. 🙂 je n'ai pas besoin d'aide, j'ai encore des choses à apprendre
6. 😎 je ne vois pas ce que je pourrais apprendre de plus
