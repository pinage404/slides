---
layout: section
---

## Problème 2

---

Qui a déjà eu ce type d'expérience ?

<v-clicks depth="2">

* Les tests ne passent pas sur ma machine
* Pourtant ils passent chez moi

_reflexion_ 🤔

* T'es en quelle version de Java ?
* 8 et toi ?
* Ah ! Moi, je suis en 14

_upgrade_ ⏳

* C'est bon : ça passe !

</v-clicks>

---

Qui a déjà eu ce type d'expérience ?

> J'ai __mis à jour__ Python et maintenant tout est __"cassé"__

---

Un problème de dépendance

---

### Types de dépendances

<v-click>

<div>

|            | Projet        | Env            |
| :--------- | :------------ | :------------- |
| Production | React 16.13.1 | NodeJS 12.22.7 |
| "Devtime"  | Jest 27.3.1   | gdb 9.2        |

</div>

</v-click>

<!--
projet : package.json

Env : Dockerfile

Env Dev : `¯\_(ツ)_/¯` non partagé

Env : global pour tout les projets
-->

---

#### Dépendance transitive

<v-click>

Jest 27.3.1 ➞ ArgParse 1.0.10

</v-click>

<!--
* package-lock.json
* yarn.lock
* Cargo.lock
-->

---

### Solution

---

#### Nix

Gestionnaire de paquets [Nix](https://nixos.org)

* `apt`
* `homebrew`
* ...

---
layout: image
image: /map_repo_size_fresh.svg
background-size: auto 90%
---

<!--
[Repology](https://repology.org/repositories/graphs)

* nombre de paquets
* paquets à jour
-->

---

##### Avantages

<v-clicks>

* déclaratif
  * quoi
  * pas comment
* reproductible
* plusieurs versions en //
* rollback "gratuit"
* atomique
* immutable

</v-clicks>

---

##### Inconvénients

<v-clicks depth="2">

* prends plus de places
  * garbage collector (`nix-gc`)
* langage Nix difficile pour faire des trucs complexes
  * je fais pas de trucs complexes `¯\_(ツ)_/¯`
* première exécution lente (download + build)
  * cache très efficient
* addictif

</v-clicks>

<!--
on gère un problème de dépendance
-->

---
src: ./nix_lang.md
---

---

#### Alternatives à Nix

D'autres solutions existent

<v-clicks>

* Container 📦 (Docker <devicon-docker />)
* Node : nvm
* Ruby : rbenv
* Python : venv
* Java : alternative (debian)
* ...

</v-clicks>

<!--
1 process = 1 container

LSP : Language Server Protocol

apprendre un nouvel outil pour chaque stack
-->
