---
---

##### `flake.nix`

```nix {all|11|2|3,5,7|all}
{
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages."${system}";
      in
      {
        devShell = pkgs.mkShell {
          packages = [ pkgs.nodejs ];
        };
      });
}
```

<!--
environnement par projet
-->

---
layout: full
style: "overflow: auto;"
---

##### `flake.lock`

```json {all|22,23,25|24}
{
  "nodes": {
    "flake-utils": {
      "locked": {
        "lastModified": 1667395993,
        "narHash": "sha256-nuEHfE/LcWyuSWnS8t12N1wc105Qtau+/OdUAjtQ0rA=",
        "owner": "numtide",
        "repo": "flake-utils",
        "rev": "5aed5285a952e0b949eb3ba02c12fa4fcfef535f",
        "type": "github"
      },
      "original": {
        "owner": "numtide",
        "repo": "flake-utils",
        "type": "github"
      }
    },
    "nixpkgs": {
      "locked": {
        "lastModified": 1673525234,
        "narHash": "sha256-fMP37VTeqSzC8JYoQJinLOnHfjriE5uKInLWJRz5K3E=",
        "owner": "NixOS",
        "repo": "nixpkgs",
        "rev": "92f9580a4c369b4b51a7b6a5e77da43720134c9f",
        "type": "github"
      },
      "original": {
        "owner": "NixOS",
        "ref": "nixpkgs-unstable",
        "repo": "nixpkgs",
        "type": "github"
      }
    },
    "root": {
      "inputs": {
        "flake-utils": "flake-utils",
        "nixpkgs": "nixpkgs"
      }
    }
  },
  "root": "root",
  "version": 7
}
```

<!--
* package-lock.json
* yarn.lock
* Cargo.lock
-->

---

##### Langage Nix

<v-clicks>

* déclaratif
  * quoi
  * pas comment
* fonctionnel & pure & lazy
* typage dynamique
* ➡️ difficile pour faire des trucs complexes

</v-clicks>
