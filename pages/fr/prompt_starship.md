---
layout: section
---

<!-- markdownlint-disable MD003 MD022 MD033 MD041 -->

## Prompt / Invite de commande

---
layout: default
---

[![starship](https://starship.rs/logo.svg)](https://starship.rs/)

<!--
prompt alternatif

cross shell

super customizable
-->

---

<SlidevVideo autoPlay controls>
<source src="https://starship.rs/demo.webm" type="video/webm" />
<source src="https://starship.rs/demo.mp4" type="video/mp4" />
</SlidevVideo>

---

[![Démo](https://starship.rs/presets/img/pastel-powerline.png)](https://starship.rs/presets/pastel-powerline.html)
