Résumé : cibler un `commit`

|                  |                                           |
| :--------------- | :---------------------------------------- |
| Hash             | `36112cd981035cb`...                      |
| Début du hash    | `3611`                                    |
| Branche          | `ma-branche`                              |
| Tag              | `mon-tag`                                 |
| Recherche        | `":/mots recherchés"`                     |
| État courant     | `HEAD`               ou               `@` |
| Branche distante | `origin/sa-branche`                       |

::right::

Modificateurs

<!-- markdownlint-disable MD013 -->

|                                                           |              |
| :-------------------------------------------------------- | :----------- |
| `#`^ième^ commits en arrière                              | `<commit>~#` |
| Commit sur la `#`^ième^ branche<br />d'un commit de merge | `<commit>^#` |
