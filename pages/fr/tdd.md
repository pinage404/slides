---
layout: section
---

#### TDD

<v-click>

Test Driven Development

[TDD Manifesto](https://tddmanifesto.com/)

</v-click>

<!--
Faire deviner le nom

**Dévelopement dirigé par les tests**

Faire deviner les 3 phases
-->

---
src: ../en/tdd/graph/all_in_one.md
transition: view-transition
---

---
src: ../en/tdd/graph/red.md
transition: view-transition
zoom: 0.85
---

---
src: ../en/tdd/graph/green.md
transition: view-transition
zoom: 0.85
---

---
src: ../en/tdd/graph/refactor.md
transition: view-transition
zoom: 1.4
---

---

Ajout de **valeur incrémentale** 📈➰
