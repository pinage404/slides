---
layout: image-left
image: /xp_feedback_loops.png
background-size: contain
---

#### Réduire la boucle de feedback ➰

[eXtreme Programming feedback loops](http://www.extremeprogramming.org/map/loops.html)
