---
favicon: 'https://pinage404.gitlab.io/favicon.ico'
addons:
  - "slidev-component-zoom"
selectable: true
canvasWidth: 700
lineNumbers: true
defaults:
  layout: center
  class: text-center
---

# CI / CD

---
src: ../../pages/fr/disclaimer_telepathe.md
---

---
layout: section
---

## Tour de table

* votre niveau de connaissance
  * honnete
  * sans jugement
* vos attentes

---
src: ../../pages/fr/niveaux.md
---

---

### Objectifs

<v-clicks>

* Montrer qu'un système de CI / CD
  __peut__ (et _devrait_) __etre simple et facile__ à mettre en place et faire évoluer
* __Démystifier__ / désacraliser
* __Inciter__ à mettre en place et faire évoluer une CI / CD
* Montrer que la CI __évolue organiquement__

</v-clicks>

---
layout: section
---

## Théorie

---

Quel est le but d'une équipe de développement ?

<v-click>

Apporter de la valeur

</v-click>

---

Comment une équipe de développement apporte de la valeur ?

<v-clicks>

En exécutant en production un programme qu'elle a créé
qui permet d'automatiser des tâches

</v-clicks>

<!--
Créer un programme (en écrivant du code) n'est qu'un __investissement__
qui ne rapporte pas de valeur, tant qu'il n'a pas été mit en production
-->

---

Mettre en production régulièrement

=

Apporter de la valeur régulièrement

[Software Crafter Manifesto](https://manifesto.softwarecraftsmanship.org)

---

### Du code à la prod

Quelles sont les différentes étapes ?

Quels sont les outils ?

<!--
Faire deviner les différentes étapes du cycle de vie de l'application,
entre le moment où on écrit une ligne de code et le moment où ça apporte de la valeur
-->

---

|      | Étape                 | Responsabilité |
| :--- | :-------------------- | :------------- |
|      | Code                  | Source         |
| CI   | Check                 | Qualité        |
| CD   | Build                 | Livrable       |
| CD   | Deploy                | Env            |
|      | Run                   | Valeur         |
|      | Monitoring / Alerting | Valeur         |

<!--
Présentation cycle de vie de l'application

Expliquer le role de chaque étapes

Préciser que ce __n'est qu'une__ façon de voir ce cycle

Liste non exaustive d'outils permettant de gérer :
[Continuous Integration](https://landscape.cncf.io/?group=projects-and-products&view-mode=card#app-definition-and-development--continuous-integration-delivery)
-->

---

### Définitions

<v-clicks depth="3">

* CI
  * Continuous Integration / Intégration Continue
* CD
  * ⚠️ CD != CD
    * Continuous Delivery / Livraison Continue
    * Continuous Deployment / Déploiement Continue

</v-clicks>

---

### Comment commencer ?

<v-clicks>

1. Écrire la procédure
1. Scripter
1. Automatiser
    1. CI
    1. CD
    1. CD
1. Améliorer en continue

</v-clicks>

---
src: ../../pages/fr/xp_loop.md
---

---
layout: section
---

## Pause

5min

---
layout: section
---

## Pratique : démo GitLab CI

<!--
[Démo](https://gitlab.com/pinage404/slides/-/tree/main/ci_cd)
-->

---
src: ../../pages/fr/devops.md
---

---
layout: section
---

## Cloture

* Tour de table des attentes

---
src: ../../pages/en/questions.html
---

---
hide: true
---

<!-- markdownlint-disable MD013 -->

| What they | Before | Transformation | After |
|:----------|:-------|:---------------|:------|
| Know      |        |                |       |
| Believe   |        |                |       |
| Feel      |        |                |       |
| Do        |        |                |       |

<!-- markdownlint-enable MD013 -->
