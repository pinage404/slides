---
favicon: 'https://pinage404.gitlab.io/favicon.ico'
addons:
  - "slidev-component-zoom"
selectable: true
canvasWidth: 700
lineNumbers: true
defaults:
  layout: center
  class: text-center
---

# CLI

<!--
accronyme ?
-->

---

* **C**ommand
* **L**ine
* **I**nterface

<!--
terminal : VSCode + Konsole
-->

---
layout: section
---

## Historique

Héritage

<v-clicks depth="2">

* années 70 : Unix
  * 1984 : projet GNU
* pas de GUI
* écran cathodique
  * **monochrome**
    * vert
* performance / octet couteux
  * légèreté < UX

</v-clicks>

---
layout: section
---

## Objectifs

<v-clicks>

* <span class="old-terminal">
  $ S'éloigner du cliché du pavé de texte vert sur fond noir
  </span>

  * (_dégueulasse_ 🤮)
* Montrer que ça **peut** (et _devrait_) **etre facile et agréable** à utiliser
* **Inciter** à utiliser les outils en CLI

</v-clicks>

<style>
.old-terminal {
  display: inline-block;
  color: lawngreen;
  background-color: black;
  font-family: 'Courier New', monospace;
  padding: 0.5em 1em;
}
</style>

<!--
plein d'outils, à vous de notez les outils qui vous intéresse
-->

---
src: ../../pages/fr/good_cli_tools.md
---

---
src: ../../pages/fr/alternative_to_gnu_cli_tools.md
---

---
src: ../../pages/fr/fish.md
---

---
src: ../../pages/fr/prompt_starship.md
---

---
src: ../../pages/fr/font_fira_code.md
---

---
src: ../../pages/en/questions.html
---

---
hide: true
---

<!-- markdownlint-disable MD013 -->

| What they | Before | Transformation | After |
|:----------|:-------|:---------------|:------|
| Know      |        |                |       |
| Believe   |        |                |       |
| Feel      |        |                |       |
| Do        |        |                |       |

<!-- markdownlint-enable MD013 -->
