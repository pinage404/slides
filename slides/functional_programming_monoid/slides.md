---
favicon: 'https://pinage404.gitlab.io/favicon.ico'
addons:
  - "slidev-component-zoom"
selectable: true
canvasWidth: 700
lineNumbers: true
defaults:
  layout: center
  class: text-center
---

# C'est quoi un monoïde ?

---

<v-clicks>

C'est un **ensemble de _trucs_**

avec une **opération**

qui respecte **3 propriétés**

</v-clicks>

---

```typescript
type Operation<Ensemble> = (a: Ensemble, b: Ensemble) => Ensemble
```

<v-click>

```typescript
const add: Operation<number> = (a: number, b: number): number => {
  return a + b
}
```

</v-click>

---
layout: section
---

## Composition

---

<v-clicks>

|  Ensemble  | Opération |           Composition           |
|:----------:|:---------:|:-------------------------------:|
|     🥗     |    🔪     |          🥬 🔪 🍅 ∈ 🥗          |
|     €      |     +     |           2€ + 3€ ∈ €           |
|     ℕ      |    `*`    |           `2 * 3` ∈ ℕ           |
| `boolean`  |   `&&`    |   `true && false` ∈ `boolean`   |
|  `string`  |    `+`    | `"Hello" + " world"` ∈ `string` |
| `number[]` | `concat`  | `[2].concat([3])` ∈ `number[]`  |

</v-clicks>

<!--
ℕ Nombre naturel

∈ appartient
-->

---
layout: section
---

## Associativité

---

<v-clicks>

| Ensemble | Opération |                     Associativité                     |
|:--------:|:---------:|:-----------------------------------------------------:|
|    🥗    |    🔪     | (🥔 🔪 🥬) 🔪 🍅 = 🥗<br/>≡<br/>🥔 🔪 (🥬 🔪 🍅) = 🥗 |
|    €     |     +     |       (2€ + 3€) + 5€<br />≡<br />2€ + (3€ + 5€)       |
|    ℕ     |    `*`    |        `(2 * 3) * 5`<br />≡<br />`2 * (3 * 5)`        |

</v-clicks>

---

<!-- markdownlint-disable MD013 -->

<v-clicks>

|  Ensemble  | Opération |                               Associativité                               |
|:----------:|:---------:|:-------------------------------------------------------------------------:|
| `boolean`  |   `&&`    |      `(true && false) && true`<br />≡<br />`true && (false && true)`      |
|  `string`  |    `+`    |  `("Hello" + " world") + " !"`<br />≡<br />`"Hello" + (" world" + " !")`  |
| `number[]` | `concat`  | `([1].concat([2])).concat([3])`<br />≡<br />`[1].concat([2].concat([3]))` |

</v-clicks>

<!-- markdownlint-enable MD013 -->

---
layout: section
---

## Élément neutre

---

<v-clicks>

|  Ensemble  | Opération |       Élément neutre       |
|:----------:|:---------:|:--------------------------:|
|     🥗     |    🔪     |       🥬 🔪 🥣 ≡ 🥬        |
|     €      |     +     |        2€ + 0€ ≡ 2€        |
|     ℕ      |    `*`    |       `2 * 1` ≡ `2`        |
| `boolean`  |   `&&`    |  `true && true` ≡ `true`   |
|  `string`  |    `+`    | `"Hello" + ""` ≡ `"Hello"` |
| `number[]` | `concat`  |  `[2].concat([])` ≡ `[2]`  |

</v-clicks>

---

Pour aller plus loin

[Domain modeling with monoids](https://blog.arolla.fr/wp-content/uploads/2018/10/DomainModelingwithMonoids.pdf)

Cyrille Martraire 2018 🗒️

---
src: ../../pages/en/questions.html
---

---
hide: true
---

<!-- markdownlint-disable MD013 -->

| What they | Before | Transformation | After |
|:----------|:-------|:---------------|:------|
| Know      |        |                |       |
| Believe   |        |                |       |
| Feel      |        |                |       |
| Do        |        |                |       |

<!-- markdownlint-enable MD013 -->
