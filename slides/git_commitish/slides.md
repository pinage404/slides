---
favicon: 'https://pinage404.gitlab.io/favicon.ico'
addons:
  - "slidev-component-zoom"
selectable: true
canvasWidth: 700
lineNumbers: false
defaults:
  layout: center
  class: text-center
---

# git subcommand `<commit>`

<!--
🍿🚿
-->

---
layout: section
---

## Introduction

---

```sh
git reset --help
```

<div class="line-splitted">

```txt
git reset [--soft | --mixed [-N] | --hard | --merge | --keep] [-q]
```

<span v-mark.box>

```txt
[<commit>]
```

</span>

</div>

<style>
  .line-splitted {
    display: grid;
    grid-template-columns: auto auto;
  }
</style>

<!--
* `[]` = optionel
* `<blabla>` = variable
-->

---
class: table-of-content
---

<Toc maxDepth="2" />

---
layout: section
---

## Cibler un commit

---

Comment cibler précisement un `commit` ?

<v-click>

Via son hash

Exemple `36112cd981035cbc3efd711aff02c67eacd9a389`

```sh
git show 36112cd981035cbc3efd711aff02c67eacd9a389
```

</v-click>

---

<div style="font-size: 2em;">

⌨️

😰

</div>

<!--
Flemme

Long à taper
-->

---

Comment cibler précisement un `commit` `*` ?

`*` _sans taper les 40 caractères du hash_

<v-click>

Via **le début** de son hash

Exemple `3611`

```sh
git show 3611
```

4 caractères _minimum_

</v-click>

---

Limite : cas ambigu

```txt
erreur : l'id court d'objet 3131 est ambigu
astuce: Les candidats sont :
astuce:   arbre 3131d642
astuce:   blob 3131d998
fatal : argument '3131' ambigu : révision inconnue ou chemin inexistant.
```

Solutions : ajouter des caractères

Exemple `3131d9`

---

🤖

---
layout: two-cols-header
layoutClass: how-to-target-this-commit
style: |
    gap: 2em
---

Comment identifier _facilement_ des commits ?

::left::

![graph](/log_graph.png)

::right::

<v-click>

* `branch`
* `tag`
* `remote branch`
* `HEAD`

</v-click>

---

Qu'est-ce qu'une `branch` ?

<v-click>

Un "pointeur **mobile**" vers un `commit`

</v-click>

---

Qu'est-ce qu'un `tag` ?

<v-click>

Un "pointeur **fixe**" vers un `commit`

</v-click>

---

Qu'est-ce que c'est `HEAD` ?

<v-click>

Un "pointeur **mobile**" vers _**le**_ `commit` qui représente l'état courant

</v-click>

---

`HEAD`

<div style="font-size: 2em;">

⌨️

🤔

</div>

<v-click>

```txt
\@    @ 
 |\  /|\
/ \  / \
```

`HEAD` == [[@]]

</v-click>

<!--
majuscule
-->

---
layout: two-cols-header
layoutClass: how-to-target-this-commit
---

Comment rechercher un `commit` ?

::left::

<v-click>

* `:/mot`
* `":/des mots recherchés"`

</v-click>

::right::

<v-click>

```sh
git show ":/des mots recherchés"
```

</v-click>

---
layout: two-cols-header
layoutClass: how-to-target-this-commit
---

Comment cibler ce `commit` ?

![graph](/head.png)

::left::

<v-click>

* `@`
* `main`
* `:/gamble`
* rien _(dans la plupart des commandes)_

</v-click>

::right::

<v-click>

```sh
git show @
git switch main
git reset :/gamble
git diff
```

</v-click>

---
src: ../../pages/fr/target_commit.md
zoom: 0.75
---

---
layout: section
---

## Modificateurs

Cibler relativement un `<commit>` par rapport à un autre `<commit>`

---
layout: two-cols-header
layoutClass: how-to-target-this-commit
---

Comment cibler ce `commit` ?

![graph](/head_previous_3.png)

_**sans** passer par la recherche_

::left::

<v-click>

* `@~3`
* `main~3`

</v-click>

::right::

<v-click>

```sh
git diff main~3
```

</v-click>

---

Cibler # `commit`s en arrière par rapport à un autre `<commit>`

| #^ième^ `commit`s en arrière | Nombre       | Répétition    |
| :--------------------------- | :----------- | :------------ |
| 0^ième^                      | `<commit>~0` | `<commit>`    |
| 1^ième^                      | `<commit>~1` | `<commit>~`   |
| 2^ième^                      | `<commit>~2` | `<commit>~~`  |
| 3^ième^                      | `<commit>~3` | `<commit>~~~` |

---

`@~5`

==

`HEAD~2~~~1` ≈≈ `(((HEAD~2)~)~)~1`

==

`HEAD~2~2~` ≈≈ `((HEAD~2)~2)~`

---
layout: two-cols-header
layoutClass: how-to-target-this-commit
---

![graph](/head_previous_1.png)

::left::

<v-click>

* `@~`
* `main~`
* `@~1`
* `main~1`

</v-click>

::right::

<v-click>

```sh
git reset --hard @~
```

</v-click>

---
layout: two-cols-header
layoutClass: how-to-target-this-commit
---

![graph](/merge_base_vieille_branche_recente_chapeau_2.png)

::left::

<v-click>

* `@^2`
* `main^2`

</v-click>

::right::

<v-click>

```sh
git switch --detach @^2
```

</v-click>

---

`<commit>~1`

==

`<commit>^1`

---

`<commit>~2`

⚠️ != ⚠️

`<commit>^2`

---

Cibler la #^ième^ `branch` par rapport à un `<commit>` de merge

| #^ième^ `branch` |              |             |
| :--------------- | :----------- | :---------- |
| 1                | `<commit>^1` | `<commit>^` |
| 2                | `<commit>^2` |             |

---

1 == 0 🤷

`<commit>^1`

==

`<commit>^0`

---
layout: two-cols-header
layoutClass: how-to-target-this-commit
---

![graph](/merge_base_vieille_branche_recente_chapeau_1.png)

::left::

<v-click>

* `@^1`
* `main^1`
* `@~1`
* `main~1`

</v-click>

::right::

<v-click>

```sh
git reset --hard @~1
```

</v-click>

---
layout: two-cols-header
layoutClass: how-to-target-this-commit
---

![graph](/merge_base_recente_branche_vieille_chapeau_2.png)

::left::

<v-click>

* `@^2`
* `main^2`

</v-click>

::right::

<v-click>

```sh
git log --oneline main^2
```

</v-click>

---
layout: two-cols-header
layoutClass: how-to-target-this-commit
---

![graph](/merge_base_recente_branche_vieille_chapeau_1.png)

::left::

<v-click>

* `@^1`
* `main^1`
* `@~1`
* `main~1`

</v-click>

::right::

<v-click>

```sh
git restore --source @~1 .
```

</v-click>

---
layout: two-cols-header
layoutClass: how-to-target-this-commit
---

![graph](/merge_no_ff_chapeau_2.png)

::left::

<v-click>

* `@^2`
* `main^2`

</v-click>

::right::

<v-click>

```sh
git branch pull-request main^2
```

</v-click>

---
layout: two-cols-header
layoutClass: how-to-target-this-commit
---

![graph](/merge_no_ff_chapeau_1.png)

::left::

<v-click>

* `@^1`
* `main^1`
* `@~1`
* `main~1`

</v-click>

::right::

<v-click>

```sh
git switch --create avant-la-pull-request main^1
```

</v-click>

---
layout: two-cols-header
layoutClass: how-to-target-this-commit
---

![graph](/merge_previous_2.png)

::left::

<v-click>

* `@^2~2`
* `main^2~2`

</v-click>

::right::

<v-click>

```sh
git diff main^2~2
```

</v-click>

---
layout: two-cols-header
layoutClass: how-to-target-this-commit
---

Comment cibler le commit de `ma-branche` ?

![graph](/ma-branche.png)

::left::

<v-click>

* `ma-branche` 😜
* `@~3^2`
* `main~3^2`

</v-click>

::right::

<v-click>

```sh
git switch ma-branche
```

</v-click>

---

Clavier ⌨️

|                 |      `~`       |      `@`       |
| :-------------- | :------------: | :------------: |
| Linux / Windows | `Alt Gr` + `2` | `Alt Gr` + `0` |
| Linux           | `Shift` + `²`  |                |
| Mac             | `Option` + `N` |      `@`       |

---
layout: two-cols
src: ../../pages/fr/target_commit.md
zoom: 0.75
---

---
src: ../../pages/en/questions.html
---

---
hide: true
---

<!-- markdownlint-disable MD013 -->

| What they | Before | Transformation | After |
|:----------|:-------|:---------------|:------|
| Know      |        |                |       |
| Believe   |        |                |       |
| Feel      |        |                |       |
| Do        |        |                |       |

<!-- markdownlint-enable MD013 -->
