---
favicon: 'https://pinage404.gitlab.io/favicon.ico'
addons:
  - "slidev-component-zoom"
selectable: true
canvasWidth: 700
lineNumbers: true
defaults:
  layout: center
  class: text-center
title: Gestion des conflits
---

<span style="font-size: 20vmax">

📖

</span>

---

```sh
git switch main
```

```python
def fizzBuzz(number: int) -> str:
    return f"{number}"
```

```sh
git add --all
git commit
```

---
layout: two-cols
style: |
    gap: 0.4ch
---

🦸‍♀️ Wanda

```sh
git switch --create fizz
```

```python
def fizzBuzz(number: int) -> str:
    if number % 3 == 0:
        return "Fizz"
    return f"{number}"
```

```sh
git commit --all
```

::right::

<v-click>

🤖 Vison

```sh
git switch --create buzz
```

```python
def fizzBuzz(number: int) -> str:
    if number % 5 == 0:
        return "Buzz"
    return f"{number}"
```

```sh
git commit --all
```

</v-click>

---

![graph](/graph_before_merge.png)

---

```sh
git switch main
git merge fizz
```

![graph](/graph_before_conflict.png)

---

```sh
git merge buzz
```

<v-clicks>

```txt
Fusion automatique de fizzbuzz.py
CONFLIT (contenu) : Conflit de fusion dans fizzbuzz.py
Pré-image enregistrée pour 'fizzbuzz.py'
La fusion automatique a échoué ; réglez les conflits et validez le résultat.
```

(╯°□°)╯︵ ┻━┻

💥💻🔥

₍₍ ᕕ(´◕⌓◕)ᕗ⁾⁾

</v-clicks>

---
layout: intro
---

<!-- markdownlint-disable-next-line MD025 -->
# Gestion des conflits

---

<span v-mark.strike="1">😠🗯️🗱🗮😡</span>

<span v-mark.strike="1">CNV</span>

<v-click at="1">

**`git`**

<small>🤡</small>

</v-click>

---
class: table-of-content
---

<Toc maxDepth="2" />

---
layout: section
---

## Qu'est-ce qu'un conflit ?

---

### Sémantique

<v-click at="1">😠🗯️🗱🗮😡</v-click>

<span v-mark.strike at="1">Conflit</span>
<v-click at="1">😱💭</v-click>

<v-click at="1">

**Divergence** 🙂

</v-click>

---

### Divergence

🚫🤖➡️🤔

<v-clicks>

* _empêchant_ la **fusion automatique** 🚫🤖
* _demandant_ une **validation humaine** 🤔

</v-clicks>

---
layout: section
---

## Quand est-ce qu'un conflit se produit ?

---

Quand on **ré-applique** un **changement de code**
qui impacte une partie du code qui a **divergé** entre :

<v-clicks>

* la `HEAD` sur laquelle le changement était basé
* la `HEAD` actuelle

</v-clicks>

---

![graph](/graph_before_conflict.png)

<!--
`buzz` est basé sur `:/common number`

`HEAD -> main`
-->

---

### Quelles commandes peuvent produire des conflits ?

<v-click>

* `merge`
  * `pull`
* `cherry-pick`
  * `rebase`
* `revert`
* `stash apply`
  * `stash pop`

</v-click>

---
layout: section
---

## À quoi ressemble un conflit ?

---

```python
 def fizzBuzz(number: int) -> str:
 <<<<<<< HEAD
     if number % 3 == 0:
         return "Fizz"
 =======
     if number % 5 == 0:
         return "Buzz"
 >>>>>>> buzz
     return f"{number}"
```

<!--
git est comme sur Linux : tout est fichier

c'est difficile à lire
donc on va décortiquer
-->

---

### Marqueurs de conflit

````md magic-move
```python
# the conflict

 <<<<<<<

 =======

 >>>>>>>

```

```python
# the conflict

 <<<<<<< HEAD

 =======

 >>>>>>> buzz

```

```python
# the conflict

 <<<<<<< HEAD (Modification actuelle)

 ======= (séparateur de la divergence)

 >>>>>>> buzz (Modification entrante)

```

```python
# the conflict

 <<<<<<< HEAD (Modification actuelle)

 code existant sur HEAD

 ======= (séparateur de la divergence)

 code ré-appliqué

 >>>>>>> buzz (Modification entrante)

```

```python
# the conflict
 def fizzBuzz(number: int) -> str:
 <<<<<<< HEAD (Modification actuelle)
     if number % 3 == 0:
         return "Fizz"
 ======= (séparateur de la divergence)
     if number % 5 == 0:
         return "Buzz"
 >>>>>>> buzz (Modification entrante)
     return f"{number}"
```
````

<!--
il y a les marqueurs

[click]
suffixé d'un nom permettant de comprendre d'où vient le bloc

[click]
en vulgarisant

[click]
les marqueurs entourent le code qui diffère

[click]
avec l'exemple de tout à l'heure
-->

---
layout: section
---

## Comment résoudre un conflit ?

---

<v-clicks>

1. Faire un choix entre les 2 versions
1. Potentiellement, faire un mélange des deux
1. Supprimer les marqueurs de conflits
1. `git add <path>`
1. `git <subcommand> --continue`
   * ou `git commit`

</v-clicks>

---

```python
def fizzBuzz(number: int) -> str:
    if number % 3 == 0:
        return "Fizz"
    if number % 5 == 0:
        return "Buzz"
    return f"{number}"
```

```sh
git add fizzbuzz.py
```

---

```sh
git merge --continue
```

<v-click>

![graph](/graph_after_merge.png)

┬─┬ノ( º _ ºノ)

</v-click>

---

<div style="font-size: 2em;">

⌨️

😰

</div>

---
layout: section
---

## Tips

---

### mergetool

[`~/.gitconfig`](https://gitlab.com/pinage404/dotfiles/-/blob/62c8337fd042dffe63d7d6ee9b011b5c3e781965/dotfiles/config/git/vscode.gitconfig#L10)

```toml
[merge]
    tool = vscode
    guitool = vscode
[mergetool "vscode"]
    cmd = code --wait --merge $REMOTE $LOCAL $BASE $MERGED
```

---

```sh
git mergetool
```

![VSCode used as git mergetool](/vscode_mergetool.png)

---

### rerere

---

**Re**_use_ **Re**_corded_ **Re**_solution_

<v-clicks>

Permet d'_en_**re**_gistrer_ une **ré**_solution_ de conflit une fois
et de la **ré**-_utiliser_ pour les fois suivantes

[delicious-insights.com/fr/articles/git-rerere/](https://delicious-insights.com/fr/articles/git-rerere/)

</v-clicks>

---
src: ../../pages/en/questions.html
---

---
hide: true
---

<!-- markdownlint-disable MD013 -->

| What they | Before | Transformation | After |
|:----------|:-------|:---------------|:------|
| Know      |        |                |       |
| Believe   |        |                |       |
| Feel      |        |                |       |
| Do        |        |                |       |

<!-- markdownlint-enable MD013 -->
