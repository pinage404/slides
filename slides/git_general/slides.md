---
favicon: 'https://pinage404.gitlab.io/favicon.ico'
addons:
  - "slidev-component-zoom"
selectable: true
canvasWidth: 700
lineNumbers: true
defaults:
  layout: center
  class: text-center
---

# `git` Général

---
class: table-of-content
---

<Toc maxDepth="2" />

---
layout: section
---

## Historique

---

### Qui ? / Quand ? / Pour quoi ?

* Créé par <v-click>Linus Torvald</v-click> en <v-click>2005</v-click>
* Maintenu par <v-click>[Junio `gitster` Hamano](https://github.com/gitster)</v-click>
* <div>Utilisé pour <v-click>le développement du kernel Linux <devicon:linux /></v-click></div>

---

### Pourquoi ?

<v-click>

BitKeeper est devenu payant 💰

</v-click>

<!--
* l'outil utilisé (avant que git existe) pour Linux
* était closed source (à l'époque)
* quelqu'un a été accusé de l'avoir rétro-engineeré pour créer un clone
* il est devenu payant
-->

---
layout: section
---

## Quoi ?

<v-click>

Système de gestion de versions

</v-click>

<!--
est-ce que vous connaissez d'autres outils ?
-->

---
zoom: 1.7
---

```mermaid
flowchart TB
    subgraph Décentralisé
        direction TB

        BitKeeper --> Git
        BitKeeper --> Mercurial
    end

    subgraph ClientServer[Client Serveur]
        direction TB

        CVS[Concurrent Versions System CVS] --> SVN[Apache Subversion SVN]
    end
```

---
zoom: 0.9
---

### Différence protocol VS service

<v-clicks>

* <div>
    Git <devicon:git />
    !=
    GitHub <skill-icons:github-light />
    & GitLab <devicon:gitlab />
    & BitBucket <devicon:bitbucket />
    & Serveur Gitea autohébergé …
  </div>
* <div>
    Email
    !=
    GMail <skill-icons:gmail-light />
    & Outlook <vscode-icons:file-type-outlook />
    & Serveur Roundcube autohébergé …
  </div>

</v-clicks>

---
src: ../../pages/en/questions.html
---

---
hide: true
---

<!-- markdownlint-disable MD013 -->

| What they | Before | Transformation | After |
|:----------|:-------|:---------------|:------|
| Know      |        |                |       |
| Believe   |        |                |       |
| Feel      |        |                |       |
| Do        |        |                |       |

<!-- markdownlint-enable MD013 -->
