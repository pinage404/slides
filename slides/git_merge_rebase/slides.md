---
favicon: 'https://pinage404.gitlab.io/favicon.ico'
addons:
  - "slidev-component-zoom"
selectable: true
canvasWidth: 700
lineNumbers: true
defaults:
  layout: center
  class: text-center
---

# `git merge` & `git rebase`

(basique)

---
class: table-of-content
---

<Toc maxDepth="2" />

---
layout: section
---

## Merge

---

```mermaid
gitGraph
    commit id:"some"
    commit id:"shared"
    commit id:"work"
    branch feature
    checkout feature
    commit id:"refactor"
    commit id:"add feature"
    checkout main
    commit id:"colleagues' work"
```

```sh
git switch main
git merge feature
```

<v-click>

```mermaid
gitGraph
    commit id:"some"
    commit id:"shared"
    commit id:"work"
    branch feature
    checkout feature
    commit id:"refactor"
    commit id:"add feature" tag:"feature"
    checkout main
    commit id:"colleagues' work"
    merge feature tag:"main"
```

</v-click>

<!--
divergent branches
-->

---

```mermaid
gitGraph
    commit id:"some"
    commit id:"shared"
    commit id:"work" tag:"main"
    branch feature
    checkout feature
    commit id:"refactor"
    commit id:"add feature" tag:"feature"
```

```sh
git switch main
git merge feature
```

<v-click>

```mermaid
gitGraph
    commit id:"some"
    commit id:"shared"
    commit id:"work"
    commit id:"refactor"
    commit id:"add feature" tag:"main feature"
    branch feature
```

</v-click>

<!--
fast forward by default
-->

---

```mermaid
gitGraph
    commit id:"some"
    commit id:"shared"
    commit id:"work" tag:"main"
    branch feature
    checkout feature
    commit id:"refactor"
    commit id:"add feature" tag:"feature"
```

```sh
git switch main
git merge --no-ff feature
```

<v-click>

```mermaid
gitGraph
    commit id:"some"
    commit id:"shared"
    commit id:"work"
    branch feature
    checkout feature
    commit id:"refactor"
    commit id:"add feature" tag:"feature"
    checkout main
    merge feature tag:"main"
```

</v-click>

<!--
no fast forward
-->

---
layout: section
---

## Rebase

---

```mermaid
gitGraph
    commit id:"some"
    commit id:"shared"
    commit id:"work"
    branch feature
    checkout feature
    commit id:"refactor"
    commit id:"add feature" tag:"feature"
    checkout main
    commit id:"colleagues' work" tag:"main"
```

```sh
git switch feature
git rebase main
```

<v-click>

```mermaid
gitGraph
    commit id:"some"
    commit id:"shared"
    commit id:"work"
    branch feature
    checkout feature
    commit id:"refactor"
    commit id:"add feature" tag:"feature"
    checkout main
    commit id:"colleagues' work" tag:"main"
    branch rebasing/feature
    cherry-pick id:"refactor"
    cherry-pick id:"add feature"
```

</v-click>

---

```mermaid
gitGraph
    commit id:"some"
    commit id:"shared"
    commit id:"work"
    branch feature
    checkout feature
    commit id:"refactor"
    commit id:"add feature" tag:"feature"
    checkout main
    commit id:"colleagues' work" tag:"main"
```

```sh
git switch feature
git rebase main
```

```mermaid
gitGraph
    commit id:"some"
    commit id:"shared"
    commit id:"work"
    commit id:"colleagues' work" tag:"main"
    branch feature
    checkout feature
    commit id:"refactor"
    commit id:"add feature" tag:"feature"
```

---
src: ../../pages/en/questions.html
---

---
hide: true
---

<!-- markdownlint-disable MD013 -->

| What they | Before | Transformation | After |
|:----------|:-------|:---------------|:------|
| Know      |        |                |       |
| Believe   |        |                |       |
| Feel      |        |                |       |
| Do        |        |                |       |

<!-- markdownlint-enable MD013 -->
