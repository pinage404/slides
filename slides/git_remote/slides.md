---
favicon: 'https://pinage404.gitlab.io/favicon.ico'
addons:
  - "slidev-component-zoom"
selectable: true
canvasWidth: 700
lineNumbers: true
defaults:
  layout: center
  class: text-center
---

# `git remote`

---
src: ../../pages/fr/ensemble_on_va_plus_loin.md
---

---
class: table-of-content
---

<Toc maxDepth="2" />

---
layout: section
---

## Théorie

---

### Quoi ?

---

_Une_ manière de **partager** des **modifications** dans un repo entre pair

<!--
[patch email](https://github.com/torvalds/linux/pull/17#issuecomment-5654674)
-->

---

remote ~= alias + tracking

<v-click>

- SSH
- HTTPS
- File
- […](https://git-scm.com/docs/git-fetch#_git_urls)

</v-click>

---

### `origin`

décentralisé

---
zoom: 1.7
---

### PR / Pull Request

```mermaid
flowchart LR
    subgraph GitHub
        subgraph Vision["🤖 Vision"]
            VisionRemote[Vision/Repo]

            Upstream>Upstream]
        end

        subgraph Wanda["🦸‍♀️ Wanda"]
            WandaRemote[Wanda/Repo]

            Fork>Fork]
        end
    end

    subgraph MachineWanda["Machine 🦸‍♀️ Wanda"]
        WandaClone[Repo]

        Clone>Clone]
    end

    WandaClone --->|"push origin"| WandaRemote
    WandaRemote -->|"Pull Request : origin"| VisionRemote
    VisionRemote -.->|"Pull : fork 🦸‍♀️ Wanda"| WandaRemote
```

<!--
on travaille en local sur un `clone`

puis on fait une `Pull Request` :
on demande à ce que les maintainers d'`upstream` `pull` notre `fork`
-->

---
zoom: 1.7
---

### MR / Merge Request

```mermaid
flowchart LR
    subgraph GitLab
        subgraph Wanda["🦸‍♀️ Wanda"]
            WandaRemote[Wanda/Repo]
        end
    end

    subgraph MachineWanda["Machine 🦸‍♀️ Wanda"]
        WandaRepo[Repo]
    end

    WandaRemote -->|"Merge Request : branch"| WandaRemote
    WandaRepo -->|"push origin"| WandaRemote
```

<!--
on travaille en local sur un `clone`

puis on fait une `Pull Request` :
on demande à ce que les maintainers `merge` notre `branch`
-->

---

```mermaid
flowchart TB
    subgraph GitLab
        direction TB

        GitLabRepo[Repo]
    end

    subgraph MachineVision["Machine 🤖 Vision"]
        direction TB

        VisionRepo[Repo]
    end

    subgraph MachineWanda["Machine 🦸‍♀️ Wanda"]
        direction TB

        WandaRepo[Repo]
        WandaRepoCopy[Repo Copy]
    end

    VisionRepo --->|"git@gilab.com:user/repo.git"| GitLabRepo
    WandaRepo --->|"httsp://gilab.com/user/repo.git"| GitLabRepo

    VisionRepo -->|"vision@wanda.net:repo_copy.git"| WandaRepoCopy

    WandaRepo -->|"wanda@vision.net:repo.git"| VisionRepo
    WandaRepo -->|"../repo_copy"| WandaRepoCopy
```

<v-click>

```sh
git push --set-upstream origin new-feature
```

</v-click>

<!--
la décentralisation permet d'interagir avec différentes `remotes`

[click]
il est possible de d'associer une `remote` par défaut pour une `branch`
-->

---
layout: section
---

## Synchronisation avec une `remote`

---
zoom: 1.35
---

<!-- markdownlint-disable MD013 -->

```mermaid
flowchart TB
    subgraph origin
        origin_F((F))
        ==> origin_E((E))
        ==> origin_D((D))
        ==> origin_C((C))
        ==> origin_B((B))
        ==> origin_A((A))

        origin_main[main]
        origin_main --> origin_E

        origin_bug[bug]
        origin_bug --> origin_F
    end

    subgraph Clone
        clone_D((D))
        ==> clone_C((C))
        ==> clone_B((B))
        ==> clone_A((A))

        clone_H((H))
        ==> clone_B

        clone_K((K))
        ==> clone_C

        main[main]
        main --> clone_D

        remotes_origin_main{{origin/main}}
        remotes_origin_main --> clone_D

        feature[feature]
        feature --> clone_H

        remotes_origin_feature{{origin/feature}}
        remotes_origin_feature --> clone_H

        remotes_origin_fix{{origin/fix}}
        remotes_origin_fix --> clone_K
    end

    remotes_origin_main -.-> origin_main

    classDef remotes stroke:red,color:red;
    class remotes_origin_main,remotes_origin_bug,remotes_origin_feature,remotes_origin_fix remotes;

    classDef commit stroke:royalblue,color:royalblue;
    class origin_A,origin_B,origin_C,origin_D,origin_E,origin_F,clone_A,clone_B,clone_C,clone_D,clone_E,clone_F,clone_H,clone_K commit;
```

<!-- markdownlint-enable MD013 -->

<v-click>

```sh
git fetch
```

</v-click>

<!--
que va faire cette commande ?
-->

---
zoom: 1.3
---

<!-- markdownlint-disable MD013 -->

```mermaid
flowchart TB
    subgraph origin
        origin_F((F))
        ==> origin_E((E))
        ==> origin_D((D))
        ==> origin_C((C))
        ==> origin_B((B))
        ==> origin_A((A))

        origin_main[main]
        origin_main --> origin_E

        origin_bug[bug]
        origin_bug --> origin_F
    end

    subgraph Clone
        clone_F((F))
        ==> clone_E((E))
        ==> clone_D((D))
        ==> clone_C((C))
        ==> clone_B((B))
        ==> clone_A((A))

        clone_H((H))
        ==> clone_B

        clone_K((K))
        ==> clone_C

        main[main]
        main --> clone_D

        remotes_origin_main{{origin/main}}
        remotes_origin_main --> clone_E

        remotes_origin_bug{{origin/bug}}
        remotes_origin_bug --> clone_F

        feature[feature]
        feature --> clone_H

        remotes_origin_feature{{origin/feature}}
        remotes_origin_feature --> clone_H

        remotes_origin_fix{{origin/fix}}
        remotes_origin_fix --> clone_K
    end

    remotes_origin_main -.-> origin_main
    remotes_origin_bug -.-> origin_bug

    classDef remotes stroke:red,color:red;
    class remotes_origin_main,remotes_origin_bug,remotes_origin_feature,remotes_origin_fix remotes;

    classDef commit stroke:royalblue,color:royalblue;
    class origin_A,origin_B,origin_C,origin_D,origin_E,origin_F,clone_A,clone_B,clone_C,clone_D,clone_E,clone_F,clone_H,clone_K commit;
```

<!-- markdownlint-enable MD013 -->

<v-click>

```sh
git fetch --prune
```

</v-click>

---
zoom: 0.9
---

<!-- markdownlint-disable MD013 -->

```mermaid
flowchart TB
    subgraph origin
        origin_F((F))
        ==> origin_E((E))
        ==> origin_D((D))
        ==> origin_C((C))
        ==> origin_B((B))
        ==> origin_A((A))

        origin_main[main]
        origin_main --> origin_E

        origin_bug[bug]
        origin_bug --> origin_F
    end

    subgraph Clone
        clone_F((F))
        ==> clone_E((E))
        ==> clone_D((D))
        ==> clone_C((C))
        ==> clone_B((B))
        ==> clone_A((A))

        clone_H((H))
        ==> clone_B

        main[main]
        main --> clone_D

        remotes_origin_main{{origin/main}}
        remotes_origin_main --> clone_E

        remotes_origin_bug{{origin/bug}}
        remotes_origin_bug --> clone_F

        feature[feature]
        feature --> clone_H
    end

    remotes_origin_main -.-> origin_main
    remotes_origin_bug -.-> origin_bug

    classDef remotes stroke:red,color:red;
    class remotes_origin_main,remotes_origin_bug remotes;

    classDef commit stroke:royalblue,color:royalblue;
    class origin_A,origin_B,origin_C,origin_D,origin_E,origin_F,clone_A,clone_B,clone_C,clone_D,clone_E,clone_F,clone_H,clone_K commit;
```

<!-- markdownlint-enable MD013 -->

<v-click>

```sh
git pull
```

</v-click>

---

<!-- markdownlint-disable MD013 -->

```mermaid
flowchart TB
    subgraph origin
        origin_F((F))
        ==> origin_E((E))
        ==> origin_D((D))
        ==> origin_C((C))
        ==> origin_B((B))
        ==> origin_A((A))

        origin_main[main]
        origin_main --> origin_E

        origin_bug[bug]
        origin_bug --> origin_F
    end

    subgraph Clone
        clone_F((F))
        ==> clone_E((E))
        ==> clone_D((D))
        ==> clone_C((C))
        ==> clone_B((B))
        ==> clone_A((A))

        clone_H((H))
        ==> clone_B

        main[main]
        main --> clone_E

        remotes_origin_main{{origin/main}}
        remotes_origin_main --> clone_E

        remotes_origin_bug{{origin/bug}}
        remotes_origin_bug --> clone_F

        feature[feature]
        feature --> clone_H
    end

    remotes_origin_main -.-> origin_main
    remotes_origin_bug -.-> origin_bug

    classDef remotes stroke:red,color:red;
    class remotes_origin_main,remotes_origin_bug remotes;

    classDef commit stroke:royalblue,color:royalblue;
    class origin_A,origin_B,origin_C,origin_D,origin_E,origin_F,clone_A,clone_B,clone_C,clone_D,clone_E,clone_F,clone_H,clone_K commit;
```

<!-- markdownlint-enable MD013 -->

---
layout: section
---

## Astuces

---

### `fetch` != `pull`

<v-clicks>

`fetch` + `merge` = `pull`

`fetch` + `rebase` = `pull --rebase`

</v-clicks>

---

[Config](https://gitlab.com/pinage404/dotfiles/-/blob/62c8337fd042dffe63d7d6ee9b011b5c3e781965/dotfiles/config/git/better_default_behavior.gitconfig#L8)

```sh
git config --global pull.rebase merges
```

---

<v-click at="1">

### `--set-upstream`

</v-click>

```sh
git push
```

<v-click at="1">

```txt
fatal : La branche courante new-feature n'a pas de branche amont.
Pour pousser la branche courante et définir la distante comme amont, utilisez

    git push --set-upstream origin new-feature

Pour que cela soit fait automatiquement pour les branches sans
suivi distant, voir "push.autoSetupRemote' dans 'git help config'.
```

</v-click>

---

[Config](https://gitlab.com/pinage404/dotfiles/-/blob/62c8337fd042dffe63d7d6ee9b011b5c3e781965/dotfiles/config/git/better_default_behavior.gitconfig#L10)

```sh
git config --global push.autoSetupRemote true
```

---

### `(fetch)` / `(push)`

```sh
git remote --verbose
```

<v-click>

```txt
origin  git@gitlab.com:pinage404/slides.git (fetch)
origin  git@gitlab.com:pinage404/slides.git (push)
```

</v-click>

---

### Repo publique

<v-click>

```sh
git remote set-url \
    origin https://gitlab.com/pinage404/slides.git
git remote set-url --push \
    origin git@gitlab.com:pinage404/slides.git
```

```sh
git remote --verbose
```

```txt
origin  https://gitlab.com/pinage404/slides.git (fetch)
origin  git@gitlab.com:pinage404/slides.git (push)
```

</v-click>

---
src: ../../pages/en/questions.html
---

---
hide: true
---

<!-- markdownlint-disable MD013 -->

| What they | Before | Transformation | After |
|:----------|:-------|:---------------|:------|
| Know      |        |                |       |
| Believe   |        |                |       |
| Feel      |        |                |       |
| Do        |        |                |       |

<!-- markdownlint-enable MD013 -->
