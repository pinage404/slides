---
favicon: 'https://pinage404.gitlab.io/favicon.ico'
addons:
  - "slidev-component-zoom"
selectable: true
canvasWidth: 700
lineNumbers: true
defaults:
  layout: center
  class: text-center
---

# Mob Programming

101

A.K.A. _Ensemble Programming_

A.K.A. _Software Teaming_

---
layout: section
---

## Objective

Align everyone with the mob basics

---
class: table-of-content
---

<Toc columns="2" maxDepth="2" />

---
layout: section
---

## What ?

---

### Definition

<v-click>💻 + 👥</v-click>
<v-click>+ 👤</v-click>
<v-click>+ 👤</v-click>
<v-click>+ 👤</v-click>
<v-click>…</v-click>

<v-clicks>

* **A group** of people (idealy an entire team)
* trying to solve the **same problem**
* in the **same time**
* in the **same space**
* using a **single computer** 💻

</v-clicks>

<!--
also good for pair programming
-->

---

### Origins

* 2011 invented at Hunter
* 2014 described by Woody Zuill from Hunter

---
layout: section
---

## Why ?

Use Collective Intelligence

<style>
@keyframes brillant {
    to {
        text-shadow: 0 0 0.3em;
    }
}

.brillant {
    animation-name: brillant;
    animation-duration: 1s;
    animation-iteration-count: infinite;
    animation-direction: alternate-reverse;
}

.idea {
    color: yellow;
}

.poor.idea {
    font-size: 0.6em;
    letter-spacing: 2rem;
    vertical-align: middle;
    opacity: 0.8;
}

.brillant.idea {
    font-style: normal;
}
</style>

🧠 🟰 <small class="poor idea">💡</small>

🧠 ➕ 🧠 🟰 <em class="brillant idea">💡</em> ✖️ <em class="brillant idea">💡</em>

---

### Share

<v-clicks>

* Knowledge
* Best Practices
* Tools
* Methodologies

</v-clicks>

---

### Learn

<v-clicks>

* together
* from each other

</v-clicks>

---

### Improve

<v-clicks depth="2">

* Code quality
* Collective code ownership
* Collaboration
* Cohesion
* Team members levels 📊
* [Truck Factor] robustness 🚛
  * Maintainability

[Truck Factor]: https://www.agileadvice.com/2005/05/15/agilemanagement/truck-factor/

</v-clicks>

<!--
harmonizes levels upwards
-->

---

### Do

instead of planning to do

🧑‍💻 > 🗺️

<!--
avoid :

* waste (lean)
* context switching
* async ping pong
-->

---
layout: section
---

## Who ?

<v-clicks>

The people who want to

Ideally, the whole team (not only the dev)

</v-clicks>

---
layout: section
---

## How many ?

<v-clicks>

Between 3 and 8

5~6 is the best (for me)

8+ maybe you should split

</v-clicks>

---
layout: section
---

## How long ?

<v-clicks>

1h minimum

2h is good

</v-clicks>

---
layout: section
---

## When ?

<v-clicks>

When people want to

Ideally, most of the work time

</v-clicks>

<!--
avoid :

* meetings
* context switching
* waste (lean)
* async ping pong
-->

---
layout: section
---

## Where ?

In a quiet room

<v-clicks>

* In the same room : easiest 😀
* Remote : easy 🙂
  * need good connexion 🛜
* Hybrid : harder 😒
  * often not cool for the remote ones
    * connexion issues 🛜
    * face-to-face people speaking at the same time 🗣️👤💬

</v-clicks>

<!--
🎤
-->

---
layout: section
---

## Stuck ?

---
src: ../../pages/en/vote.md
---

---
layout: section
---

## When not ?

<v-clicks>

Tasks with a very long feedback loop

Unclear problem

</v-clicks>

<!--
build in CI
-->

---
layout: section
---

## How ?

---

### Live Coding 🧑‍🏫

🧑‍💻

<v-clicks>

* 😴
* 📱
* 😖
* 🗣️👤💬🌄

</v-clicks>

<v-click>

🙁

</v-click>

---
src: ../../pages/en/strong_style.md
---

---

#### Driver ⌨️

<v-click>

**Types** _only_ what the Navigator 🔀 requests

`Idea 💡 -> Code {}`

</v-click>

---

#### Navigator 🔀

<v-click>

**Chooses an idea** (from the Mobbers 💡 or his own)

`Ideas 💡💡 -> Idea 💡`

</v-click>

<v-click>

and gives it with the _highest level of abstraction_
(possible at this time)
to the Driver ⌨️

</v-click>

<!--
lower level when needed
-->

---

#### Mobber 💡

<v-click>

Suggest ideas

`🤔 -> Ideas 💡💡`

</v-click>

---

Why should we bother **strictly respecting** these roles ?

<v-click at="1">
🤔 ➡ 💡
</v-click>
<v-click at="2">
➡ 🗣️💬
</v-click>
<v-click at="3">
➡ 🔀 ➡ 🗣️💬 ➡ 🧑‍💻
</v-click>
<v-click at="4">
➡ ⌨️ ➡ `{}`
</v-click>

<v-click at="1">

Ideas 💡 must

</v-click>

<v-click at="2">

be **clearly communicated** 🗣️💬

</v-click>

<v-click at="3">

in order to to be **propery understood** (🔀 & 🧑‍💻)

</v-click>

<v-click at="4">

to move from head 🤔 to code `{}`

</v-click>

---

Questions about these roles ?

---

#### Facilitator 🥷

<v-click>

Try to make the team work well

`⚙️⚙ -> 🙂`

</v-click>

<!--
good facilitation is invisible
-->

---

### Organic

<v-clicks>

Without strict rules

_Can_ work with group who :

</v-clicks>

<v-clicks>

* know how to work with each other
* are used to working together
* are used to working in mob programming
* have the same level

</v-clicks>

---

### Timer ⏳

AKA _Randori_

Rotation of roles based on a timer

<v-clicks>

* small turn
  * 2~6min/turn
* big turn
  * each participant should have played both roles within 30min
* only one person is the timekeeper

</v-clicks>

---

### Other modes

Another time

---
layout: section
---

## Tools

<v-clicks depth="2">

* [MobTime]: shared Timer ⏳
* Microsoft [Live Share]:
  realtime collaborative development (like Google Doc but for code)
  * 1 person must host
  * the others can use a simple web browser
  * best used with real installation

</v-clicks>

[MobTime]: https://mobtime.hadrienmp.fr
[Live Share]: https://visualstudio.microsoft.com/fr/services/live-share/

---

<v-clicks>

* [tmate](https://tmate.io/): share terminal with SSH (for `vim`, `emacs`... users)
* [ngrok](https://ngrok.com/): share local port througth internet
* [mob.sh](https://mob.sh): small tool over git
* [Mobster](http://mobster.cc/): local timer
* JetBrains [Code With Me](https://www.jetbrains.com/code-with-me/)
* [Cyber-Dojo](https://cyber-dojo.org): online plateforme for dojo with many kata

</v-clicks>

---
layout: section
---

## External Resources

---

* Talks
  * 🇫🇷 [Ensemble Programming Toolbox] (Thomas Carpaye et Hadrien Mens-Pellen)
  * 🇫🇷 [Coder seul·e == vélocité optimale ?] (Adrien Joly)
  * 🇫🇷 [Le Mob programming : le meilleur moyen de partager les connaissances]
    (Hugo Voisin)
* Definition
  * [Pair Programming]
  * [Mob Programming]

[Ensemble Programming Toolbox]: https://youtu.be/c_oW0yJWveQ
[Coder seul·e == vélocité optimale ?]: https://youtu.be/gjXk7VP1fuw
[Le Mob programming : le meilleur moyen de partager les connaissances]: https://youtu.be/H_3_t4ZxPfk
[Pair Programming]: https://www.agilealliance.org/glossary/pairing/
[Mob Programming]: https://www.agilealliance.org/glossary/mob-programming/

---

* Meetups
  * 🇫🇷 🏢 [Dojo Developpement Paris] 1/week
  * 🇫🇷 🛜 [Mob Programming Francophone] 1/month
  * 🇫🇷 🛜 [Software Crafters Lyon] mob session 1/month
  * 🇫🇷 [Okiwi - Software Craftsmanship Bordeaux]

[Dojo Developpement Paris]: https://www.meetup.com/fr-FR/dojo-developpement-paris/
[Mob Programming Francophone]: https://www.meetup.com/fr-FR/paris-mob-programming/
[Software Crafters Lyon]: https://www.meetup.com/fr-FR/software-craftsmanship-lyon/
[Okiwi - Software Craftsmanship Bordeaux]: https://www.meetup.com/fr-FR/software-craftsmanship-bdx/

---

* [Awesome list](https://github.com/mobtimeapp/awesome-mobbing)
* [Mob Programming website](https://mobprogramming.org/)
* [Ensemble RPG](https://ensemble-rpg.onrender.com/): play to discover roles by mobbing
* Do we loose time ?
  * [No](https://twitter.com/d_stepanovic/status/1379451260638785536)
  * [Explaining team flow](https://youtu.be/bhpQKA9XYcE) (Michel Grootjans)

---
src: ../../pages/en/questions.html
---

---
hide: true
---

<!-- markdownlint-disable MD013 -->

| What they | Before | Transformation | After |
|:----------|:-------|:---------------|:------|
| Know      |        |                |       |
| Believe   |        |                |       |
| Feel      |        |                |       |
| Do        |        |                |       |

<!-- markdownlint-enable MD013 -->
