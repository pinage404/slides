---
favicon: 'https://pinage404.gitlab.io/favicon.ico'
addons:
  - "slidev-component-zoom"
selectable: true
canvasWidth: 700
lineNumbers: true
defaults:
  layout: center
  class: text-center
---

# Facilitator Tips Mob Programming

<!--
What was said previously ?
-->

---
layout: section
hideInToc: true
---

## Facilitator 🥷 role

<v-click>

Try to make the team work well

</v-click>

---

Make them **feel at ease**

---
src: ../../pages/en/questions.html
---

---

🤡

---
layout: section
---

## Objective

Give tips for facilitating a group

---
class: table-of-content
---

<Toc maxDepth="2" />

---

### Minimum Framework

<v-click>

Try tips incrementally

</v-click>

<v-clicks>

* **Retro** ➰🔙 _Mandatory_
* **Break** ⏸️
* **Strong Style** 👥 _Recommended_

</v-clicks>

---
class: section
---

## Principles

---

### Retro ➰🔙

---

#### Short Retro ⏸️➰🔙

Often

<v-clicks>

* After each big turn
* Every 30min
* Before/After a break

</v-clicks>

---

##### Ask

<v-clicks depth="2">

* How do they **feel** ?
  * Not what they **think** about the solution 🚫🧠
  * Which **emotion** ? 😃 😢 😱 😠
  * I **pass** 🙅
  * How do you make them **feel at ease** ? 🫥➡️😃
* Do they want to **change** anything in the **organization** ?
* …

</v-clicks>

<v-click>

Do **not argue** about the solution

</v-click>

<!--
[click]
ask for emotion => get what they think

[click]
[click]
[click]
[click]
[click]
changing timer duration

changing mode

changing constraint

[click]
or any other question
-->

---

What can be done to ensure they **leave satisfied** ? 😃

<v-clicks depth="2">

* Question ❓ about the solution
  * Not the moment ⏸️➰
  * How do you make them **feel at ease** ? 🫥➡️😃
  * Encourage questions during iteration ⏯️➰❓

</v-clicks>

<!--
maybe not the best moment

idea 💡

target 🎯

change organization

timebox 🗣️

[click]
not know ok to ask question
-->

---

Which **target** do they want to achieve in the **next iteration** ? ⏭️➰🎯

<!--
maybe not the best moment
-->

---

#### Long Retro ⏹️➿🔙

<v-clicks depth="2">

* At the end of the session
* Ask
  * What did they **learn** ? 🧑‍🎓
  * What did they **like** ? 😃
  * What did they **dislike** ? 😒
  * What can be **improved** ? 🤔
  * …

</v-clicks>

---

### Encourage 🙌

But don't force 🚫💪

<v-click>

_It's called **consent**_

</v-click>

<v-clicks>

* Participating
* Taking a role

</v-clicks>

---

### Mobility Principle 👣

<v-clicks depth="3">

* Arriving late 👣 is ok
* Going away 👣 is ok
  * break ⏸️
  * interruption
    * emergency 🔥
    * phone call 📳
    * delivery 📦
    * …

</v-clicks>

<!--
open forum

be courteous : let them know
-->

---

### Safety ⛑️

People need to **feel safe** in the group

<v-clicks depth="2">

* Psychologically & Emotionnally
  * Right to make **mistakes**
  * **Support** / mutual aid
  * **Open-Mindedness**
  * **Listen**
  * **Courage**

</v-clicks>

<!--
[click]
[click]
try

[click]
learn together

learn from each other

[click]
[click]
[click]
courage to experiment

courage to say when feeling not good

courage to say when don't understand
-->

---

### Facilitate by example

<!--
show your weakness
-->

---

### Egoless Programming

<v-clicks>

> You are not your code

<br />

> Treat people who know less than you
> with respect, deference, and patience

<br />

> Critique code instead of people :
> be kind to the coder,
> not to the code

_The Psychology of Computer Programming_ 📖 (Jerry Weinberg, 1971)

</v-clicks>

---
class: section
---

## Situations

---

### Some Mobbers 💡 don't speak 🙊 ?

<v-click>

Whisper to them or go elsewhere and ask :

</v-click>

<v-clicks depth="2">

* Do they **feel at ease** ?
* **What** do they **understand** ?
* What are **their ideas ?**
  * If they have any, **encourage** them to **suggest it**
  * Else, **encourage** them to **take some role**

</v-clicks>

---

### Some don't dare to assume a role ?

---

Remind them that :

<v-clicks>

* the roles are pressure-free
* we are not here to judge 🧑‍⚖️ but to solve a problem
* it is best to let the group carry you along

</v-clicks>

---

#### Driver ⌨️

Only has to :

<v-clicks>

* type what the Navigator 🔀 requests
* must not decide
* ask Question ❓ while they doesn't know what to type

</v-clicks>

---

#### Navigator 🔀

Only has to :

<v-clicks>

* ask and collect Mobbers 💡 ideas
* choose one

</v-clicks>

<!--
moderator
-->

---

##### Abstraction

Talk with the **highest level of abstraction**

(_possible at this time_)

<!--
faster

test with 3

fake it

extract function

say the keys on the keyboard
-->

---

##### Line number

<v-clicks>

Use line number

Use half line number to create a new one

</v-clicks>

---

```python
def foo(bar):
    return bar
```

<v-click>

```python
def foo(bar):
    if bar == "foo": return "baz"
    return bar
```

</v-click>

<!--
Navigator 🔀 Fake it !

Driver ⌨️ How ?

Navigator 🔀 Add an `if` in `1.5`

Driver ⌨️ Ok !
-->

---

### Someone feels lost ?

---

#### Ensure that everyone understand

<v-clicks depth="2">

* What should be done
  * on the whole ?
  * in the next step ?
* What has been done ?

</v-clicks>

---

#### Smaller target 🎯

---

#### Make a ToDo List 📝

<v-clicks>

* Prioritize
* Divide the current step
  * as small as needed
  * to make it understandable to everyone
* Stay focused on the current task
* Note ideas 💡
  * that are not needed to the current task

</v-clicks>

---

#### Remind the target 🎯

---

#### Take breaks ⏸️

<v-clicks>

* Communication is exhausting
* Take breaks **before** feeling tired
* Try **Pomodoro** 🍅

</v-clicks>

<!--
take sufficiently long breaks
-->

---

### Someone doesn't respect the role ?

<v-clicks>

* Remind the role
* Remember that we **respect** a framework for the **well-being** of the **group**
* They can **propose changes** to the framework, but the _group must agree_
* If they continue not to **respect** the **well-being** of the **group**,
  they must **leave** the group ➡️🚪

</v-clicks>

<!--
i need to improve
-->

---

### Someone talks too much 🗣️ ?

<v-clicks>

* Remind the target 🎯
* Remind the role
* Rotate the role
* Reminder that this is a working **group**

</v-clicks>

<!--
i need to improve
-->

---
src: ../../pages/en/feel_stucks.md
---

---
layout: section
---

## Random Tips

---

### Rotate 🔁

Often

---

### Silence 🙊

<v-clicks depth="2">

* Short are good ➡ 🤔
* Too long are bad
  * Distracted 📱
  * Passive 😴
  * Stuck 🤷
  * Frustrated 😖

</v-clicks>

---

A well-functioning group is aligned with the target 🎯

---

### Timebox ⏱️

When jumping in the unknown 🕳️

---

### How to choose the Hoster 🧑‍🔧 ?

<v-clicks>

* With the best computer 💻 / connection 🛜
* That will be availlable for a long time

</v-clicks>

---

### Experiment 🧪

⚗️🧫🔬

<v-clicks>

* new idea
* new tool
* new organisation
* new constraint

</v-clicks>

---

### Enjoy 😃

Have fun 🥳

<!--
it is unusual to have jobs that allow this type of organization
-->

---
layout: section
---

## External Resources

---

* Talks
  * 🇫🇷 [Ensemble Programming Toolbox] (Thomas Carpaye et Hadrien Mens-Pellen)
* 🇫🇷 [Qu'est-ce qu'un facilitateur ?] (Le Laptop)
* 🇫🇷 [Les étapes de la facilitation] (Le Laptop)
* [Ensemble RPG]: play to discover roles by mobbing
* [Good facilitation becomes invisible because …]

[Ensemble Programming Toolbox]: https://youtu.be/c_oW0yJWveQ
[Qu'est-ce qu'un facilitateur ?]: https://youtu.be/lb4AwgPvyu0
[Les étapes de la facilitation]: https://youtu.be/SEQJn1KgiKw
[Ensemble RPG]: https://ensemble-rpg.onrender.com/
[Good facilitation becomes invisible because …]: https://twitter.com/malk_zameth/status/1657079894310805511

---
src: ../../pages/en/questions.html
---

---
hide: true
---

<!-- markdownlint-disable MD013 -->

| What they | Before | Transformation | After |
|:----------|:-------|:---------------|:------|
| Know      |        |                |       |
| Believe   |        |                |       |
| Feel      |        |                |       |
| Do        |        |                |       |

<!-- markdownlint-enable MD013 -->
