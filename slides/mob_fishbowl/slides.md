---
favicon: 'https://pinage404.gitlab.io/favicon.ico'
addons:
  - "slidev-component-zoom"
selectable: true
canvasWidth: 700
lineNumbers: true
defaults:
  layout: center
  class: text-center
---

# Fishbowl Mob Programming

🪑🪑

---
layout: section
---

## Objective

Discover a method of organization

<v-clicks>

very fun

but only feasible in physics

</v-clicks>

<!--
if you know how to adapt it at remote locations, i'm interested
-->

---
class: table-of-content
---

<Toc maxDepth="2" />

---
layout: section
---

## "Politic" Game

<hr class="gradient" />
<SlideAltenate behavior="alternate">⬆️</SlideAltenate>

<v-click at="2">Benevolent</v-click> <v-click at="1">Dictatorship</v-click>

<v-click at="3">

* <v-click at="4">Navigator 🔀 = </v-click>
  <v-click at="5"><em>Benevolent</em> Dictator 🧑‍⚖️🫅</v-click>
* <v-click at="6">Driver ⌨️ = </v-click>
  <v-click at="7">Army / Executor 🫡🙇</v-click>
* <v-click at="8">Mobbers 💡 = </v-click>
  <v-click at="9">Population 👥</v-click>

</v-click>

<style>
.gradient {
    border: none;
    background-image:
        linear-gradient(to right, hsl(30,100%,50%), hsl(260,100%,60%), hsl(225,100%,50%));
    height: 1em;
    border-radius: 0.5em;
}
</style>

---

### An Unstable Regime

Disagree ⇒ <v-click>(_Benevolent_) Coup d'État</v-click>

<v-clicks>

To stay longer,
ask the ~~population~~ _Mobbers_ 💡 for their ideas,
then choose one

Or ignore them, you are a dictator 😈

</v-clicks>

---
layout: section
---

## Setup

<v-clicks>

🪑🪑<br />2 chairs for the Navigator 🔀

🪑🪑<br />2 chairs for the Driver ⌨️

🪑🪑🪑🪑🪑🪑<br />As many chairs as needed for the Mobbers 💡

A **video projector** or a large screen 🖵

</v-clicks>

---
layout: iframe-center
url: "https://pinage404.gitlab.io/slides/mob_fishbowl/fishbowl/index.html"
---

---
layout: section
---

## Rules

---

For each role

There **must always** be _at least_ **1 chair available**🪑

---

When someone wants to take a role :

<v-clicks>

1. Sit in the chair of that role 🪑
2. The previous person must return ➡️ with the Mobbers 💡

</v-clicks>

---

When someone wants to stop a role :

1. Just return ➡️ with the Mobbers 💡

---

When both **chairs** in a role are **empty** 🪑🪑

<v-click>

The group is **stuck** ⚠️

</v-click>

---
src: ../../pages/en/feel_stucks.md
---

---

When someone has a **question** ❓ to ask

/ does not understand something :

<v-clicks>

1. stand up
2. the group must stop until the question is answered

</v-clicks>

---
layout: section
---

## Facilitator Tips

---

Some people take the role of Navigator 🔀 too often ?

<v-clicks>

Introduce a totem 🙊
that prevents the owner from taking on this role while we have it

When a Navigator 🔀 is revoked or leaves on its own, give the totem 🙊

</v-clicks>

---
src: ../../pages/en/questions.html
---

---
hide: true
---

<!-- markdownlint-disable MD013 -->

| What they | Before | Transformation | After |
|:----------|:-------|:---------------|:------|
| Know      |        |                |       |
| Believe   |        |                |       |
| Feel      |        |                |       |
| Do        |        |                |       |

<!-- markdownlint-enable MD013 -->
