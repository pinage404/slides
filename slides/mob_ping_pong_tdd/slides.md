---
favicon: 'https://pinage404.gitlab.io/favicon.ico'
addons:
  - "slidev-component-zoom"
selectable: true
canvasWidth: 700
lineNumbers: true
defaults:
  layout: center
  class: text-center
---

# Ping Pong TDD 🏓<br /> Mob Programming

Works well with Pair Programming

---
layout: section
---

## Objective

Discover a method of organization based on tests

---
class: table-of-content
---

<Toc />

---
layout: section
---

## TDD

---

Test Driven Development

<!--
Development Driven by the Tests
-->

---
src: ../../pages/en/tdd/graph/all_in_one.md
zoom: 0.95
---

---
layout: section
---

## Ping Pong TDD 🏓

---

* 👩🏻‍💻 Alice
* 👨🏾‍💻 Bob
* 👩🏿‍💻 Charlie

---
zoom: 1.7
transition: view-transition
---

<!-- markdownlint-disable MD013 -->

Turn 1

```mermaid
flowchart LR
    %% relations

    subgraph Red
        direction LR

        RedDriver --- RedA
        RedNavigator --- RedB
        RedMobber --- RedC
    end

    subgraph Green
        direction LR

        GreenDriver --- GreenB
        GreenNavigator --- GreenC
        GreenMobber --- GreenA
    end

    start((" ")) -->|test a new behavior| Red
    Red -->|"make it pass 🔁"| Green
    %%Green -->|refactor| Green
    %% broken see https://github.com/mermaid-js/mermaid/issues/5282 and https://github.com/mermaid-js/mermaid/issues/5036
    Green --> finish((" "))

    %% styling
    RedDriver["Driver ⌨️"]
    RedNavigator["Navigator 🔀"]
    RedMobber["Mobber 💡"]

    GreenDriver["Driver ⌨️"]
    GreenNavigator["Navigator 🔀"]
    GreenMobber["Mobber 💡"]

    RedA["👩🏻‍💻 Alice"]
    RedB["👨🏾‍💻 Bob"]
    RedC["👩🏿‍💻 Charlie"]

    GreenA["👩🏻‍💻 Alice"]
    GreenB["👨🏾‍💻 Bob"]
    GreenC["👩🏿‍💻 Charlie"]

    classDef Dark fill:#0008,color:white,stroke:transparent
    class RedDriver,RedNavigator,RedMobber,GreenDriver,GreenNavigator,GreenMobber,RedA,RedB,RedC,GreenA,GreenB,GreenC Dark

    classDef Red fill:darkred,color:white
    class Red Red

    classDef Green fill:darkgreen,color:white
    class Green Green
```

---
zoom: 1.7
transition: view-transition
---

Turn 2

```mermaid
flowchart LR
    %% relations

    subgraph Red
        direction LR

        RedDriver --- RedB
        RedNavigator --- RedC
        RedMobber --- RedA
    end

    subgraph Green
        direction LR

        GreenDriver --- GreenC
        GreenNavigator --- GreenA
        GreenMobber --- GreenB
    end

    start((" ")) -->|test a new behavior| Red
    Red -->|"make it pass 🔁"| Green
    %%Green -->|refactor| Green
    %% broken see https://github.com/mermaid-js/mermaid/issues/5282 and https://github.com/mermaid-js/mermaid/issues/5036
    Green --> finish((" "))

    %% styling
    RedDriver["Driver ⌨️"]
    RedNavigator["Navigator 🔀"]
    RedMobber["Mobber 💡"]

    GreenDriver["Driver ⌨️"]
    GreenNavigator["Navigator 🔀"]
    GreenMobber["Mobber 💡"]

    RedA["👩🏻‍💻 Alice"]
    RedB["👨🏾‍💻 Bob"]
    RedC["👩🏿‍💻 Charlie"]

    GreenA["👩🏻‍💻 Alice"]
    GreenB["👨🏾‍💻 Bob"]
    GreenC["👩🏿‍💻 Charlie"]

    classDef Dark fill:#0008,color:white,stroke:transparent
    class RedDriver,RedNavigator,RedMobber,GreenDriver,GreenNavigator,GreenMobber,RedA,RedB,RedC,GreenA,GreenB,GreenC Dark

    classDef Red fill:darkred,color:white
    class Red Red

    classDef Green fill:darkgreen,color:white
    class Green Green
```

---
zoom: 1.7
transition: view-transition
---

Turn 3

```mermaid
flowchart LR
    %% relations

    subgraph Red
        direction LR

        RedDriver --- RedC
        RedNavigator --- RedA
        RedMobber --- RedB
    end

    subgraph Green
        direction LR

        GreenDriver --- GreenA
        GreenNavigator --- GreenB
        GreenMobber --- GreenC
    end

    start((" ")) -->|test a new behavior| Red
    Red -->|"make it pass 🔁"| Green
    %%Green -->|refactor| Green
    %% broken see https://github.com/mermaid-js/mermaid/issues/5282 and https://github.com/mermaid-js/mermaid/issues/5036
    Green --> finish((" "))

    %% styling
    RedDriver["Driver ⌨️"]
    RedNavigator["Navigator 🔀"]
    RedMobber["Mobber 💡"]

    GreenDriver["Driver ⌨️"]
    GreenNavigator["Navigator 🔀"]
    GreenMobber["Mobber 💡"]

    RedA["👩🏻‍💻 Alice"]
    RedB["👨🏾‍💻 Bob"]
    RedC["👩🏿‍💻 Charlie"]

    GreenA["👩🏻‍💻 Alice"]
    GreenB["👨🏾‍💻 Bob"]
    GreenC["👩🏿‍💻 Charlie"]

    classDef Dark fill:#0008,color:white,stroke:transparent
    class RedDriver,RedNavigator,RedMobber,GreenDriver,GreenNavigator,GreenMobber,RedA,RedB,RedC,GreenA,GreenB,GreenC Dark

    classDef Red fill:darkred,color:white
    class Red Red

    classDef Green fill:darkgreen,color:white
    class Green Green
```

---
src: ../../pages/en/questions.html
---

---
hide: true
---

<!-- markdownlint-disable MD013 -->

| What they | Before | Transformation | After |
|:----------|:-------|:---------------|:------|
| Know      |        |                |       |
| Believe   |        |                |       |
| Feel      |        |                |       |
| Do        |        |                |       |

<!-- markdownlint-enable MD013 -->
