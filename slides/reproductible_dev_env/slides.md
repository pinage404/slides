---
favicon: 'https://pinage404.gitlab.io/favicon.ico'
addons:
  - "slidev-component-zoom"
selectable: true
canvasWidth: 700
lineNumbers: true
defaults:
  layout: center
  class: text-center
---

# Gérer son environnement

sans prise de tete

---
layout: section
hideInToc: true
class: table-of-content
---

## Organisation

<Toc minDepth="2" maxDepth="2" />

---
layout: section
---

## Introduction

* Disclaimer
* Tour de table
  * vos attentes

<!--
> je ne suis pas Charles Xavier / Professor X : je ne suis pas télépathe
>
> donc si vous ne dites pas que vous ne comprennez pas, je ne peux pas le savoir
>
> il n'y a aucune questions stupides
>
> si vous ne dites pas que vous ne savez pas, vous n'apprendrez pas
>
> n'hésitez pas à demander tant que vous n'avez pas compris
-->

---

### Objectifs

<v-clicks>

* Faire découvrir de __nouvelles possibilités__
* Montrer qu'un environnement peut se gérer __simplement__
* __Inciter__ à mettre en place

</v-clicks>

---
src: ../../pages/fr/direnv.md
---

---
src: ../../pages/fr/demo.md
---

---
src: ../../pages/fr/nix.md
---

---
src: ../../pages/fr/compose_tools.md
---

---
src: ../../pages/fr/devbox.md
---

---
src: ../../pages/fr/compose_tools_devbox.md
---

---
layout: section
---

## Résumé

2 logiciels (DirEnv + DevBox) pour gérer son environnement sans prise de tete

---
src: ../../pages/fr/demo.md
---

---
layout: section
---

## Cloture

Tour de table des attentes

---
src: ../../pages/en/questions.html
---

---
hide: true
---

<!-- markdownlint-disable MD013 -->

| What they | Before | Transformation | After |
|:----------|:-------|:---------------|:------|
| Know      |        |                |       |
| Believe   |        |                |       |
| Feel      |        |                |       |
| Do        |        |                |       |

<!-- markdownlint-enable MD013 -->
