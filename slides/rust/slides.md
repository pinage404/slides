---
favicon: 'https://pinage404.gitlab.io/favicon.ico'
addons:
  - "slidev-component-zoom"
selectable: true
canvasWidth: 700
lineNumbers: true
defaults:
  layout: center
  class: text-center
---

# Rust

---
layout: section
---

## Disclaimer

<v-clicks>

* intro
* à l'arrache
* noob

</v-clicks>

---
layout: section
---

## Objectifs

<v-clicks>

* donner envie de s'intéresser à Rust
* faire découvrir de nouvelles possibilités
* réutiliser les patterns de Rust

</v-clicks>

---

🦀 ≠ ☢️

<v-click>

`XYZ rust lang`

</v-click>

---
layout: section
---

## 🦀

---
layout: image
image: https://www.rust-lang.org/logos/rust-logo-blk.svg
style: |
    background-size: contain;
    background-color: #777
---

---
style: |
    font-family: monospace
---

🦀 🦐

<VSwitch>
<template #0>

&nbsp;

</template>
<template #1>

Crustacean

</template>
<template #2>

C**RUST**acean

</template>
<template #3>

~~c~~**RUST**acean

</template>
<template #4>

&nbsp;Rustacean

</template>
<template #5>

&nbsp;Rustacean

</template>
</VSwitch>

<!--
vite
-->

---
layout: image
image: https://www.rustacean.net/assets/rustacean-orig-noshadow.svg
style: |
    background-position: contain;
    background-size: auto 20vmin;
    background-position: center 60%
---

Ferris

<!--
mascotte officieuse
-->

---
layout: section
---

## 🧠 Mémoire

---
class: ''
---

<table>
    <thead>
        <tr>
            <th></th>
            <th>Manual Memory Management</th>
            <th>Garbage Collector 🗑️</th>
        </tr>
    </thead>
    <tbody>
        <tr v-click="0">
            <th>Langages</th>
            <td>C / C++</td>
            <td>JS, Python, Java, Ruby, PHP…</td>
        </tr>
        <tr v-click="1">
            <th>Complexité 🤯</th>
            <td>Dev 🧑‍💻</td>
            <td>Runtime 🤖</td>
        </tr>
        <tr v-click="2">
            <th>Problème 😥</th>
            <td>Fuite mémoire 📈</td>
            <td>Performance 📉</td>
        </tr>
        <tr v-click="2">
            <td></td>
            <td><code>SEGFAULT</code></td>
            <td>▶️⏸️🚮▶️🔄</td>
        </tr>
    </tbody>
</table>

<!--
rappel fonctionnement de l'allocation mémoire
pour les gens qui n'ont jamais fait de `c`
-->

---

```javascript
function f() {
  const x = {};
  const y = {};
  x.a = y; // x references y
  y.b = x; // y references x

  return "azerty";
}

f();
```

---
src: ../../pages/fr/contraintes_vs_garanties.md
---

---

Contrainte : mécanisme de gestion de la mémoire compile time 🤔

<v-click>

Garantie : pas de `SEGFAULT` 😌 ni Garbage Collector 🗑️

</v-click>

---

```rust
fn foo() {
    let pencil = String::from("✏️");

    display_stuff(pencil);

    dbg!(pencil); // 💥
}
```

<v-click>

> Donner, c'est donner ;
> <br />
> reprendre, c'est voler

</v-click>

---

```rust
fn foo() {
    let pencil = String::from("✏️");

    display_stuff(&pencil);
    //            ^

    dbg!(pencil); // ✅
}
```

<v-clicks>

> Je te prête mon crayon,
> <br />
> mais il s'appelle "reviens"

[Ownership / Borrow (empreinter) Checker](https://en.wikipedia.org/wiki/Rust_%28programming_language%29#Ownership_and_lifetimes)

</v-clicks>

---

Rust ?

<v-click>

🔴 rouille

</v-click>

---
layout: section
---

## Modélisation

---
layout: default
transition: view-transition
zoom: 0.8
---

<div view-transition-name="success">

```typescript
type Query = {
  isLoading: boolean;
  isSuccess: boolean;
  data?: string;
  hasError: boolean;
  error?: Error;
};
```

</div>

<div view-transition-name="query">

```typescript
const query: Query = http.get("https://example.com");
```

</div>

<div view-transition-name="result">

```typescript
if (query.isLoading) console.log("is loading");
else if (query.isSuccess) console.log(query.data);
else if (query.hasError) console.error(query.error);
```

</div>

<v-click>

```typescript
const query: Query = {
  isLoading: true,
  isSuccess: true,
  hasError: true,
};
```

</v-click>

---
layout: default
transition: view-transition
zoom: 0.8
---

<div class="columns">

<div view-transition-name="loading">

```typescript
type LoadingState = {
  isLoading: true;
  isSuccess: false;
  data?: never;
  hasError: false;
  error?: never;
};
```

</div>

<div view-transition-name="success">

```typescript
type SuccessState = {
  isLoading: false;
  isSuccess: true;
  data: string;
  hasError: false;
  error?: never;
};
```

</div>

<div view-transition-name="error">

```typescript
type ErrorState = {
  isLoading: false;
  isSuccess: false;
  data?: never;
  hasError: true;
  error: Error;
};
```

</div>

</div>

<div view-transition-name="type">

```typescript
type Query = LoadingState | SuccessState | ErrorState;
```

</div>

<div view-transition-name="query">

```typescript
const query: Query = http.get("https://example.com");
```

</div>

<div view-transition-name="result">

```typescript
if (query.isLoading) console.log("is loading");
else if (query.isSuccess) console.log(query.data);
else if (query.hasError) console.error(query.error);
```

</div>

---
layout: default
transition: view-transition
zoom: 0.8
---

```typescript
enum State {
  LOADING,
  SUCCESS,
  ERROR,
}
```

<div class="columns">

<div view-transition-name="loading">

```typescript
type LoadingState = {
  state: State.LOADING;
  data?: never;
  error?: never;
};
```

</div>

<div view-transition-name="success">

```typescript
type SuccessState = {
  state: State.SUCCESS;
  data: string;
  error?: never;
};
```

</div>

<div view-transition-name="error">

```typescript
type ErrorState = {
  state: State.ERROR;
  data?: never;
  error: Error;
};
```

</div>

</div>

<div view-transition-name="type">

```typescript
type Query = LoadingState | SuccessState | ErrorState;
```

</div>

<div view-transition-name="query">

```typescript
const query: Query = http.get("https://example.com");
```

</div>

<div view-transition-name="result">

```typescript
switch (query.state) {
  case State.LOADING: console.log("is loading"); break;
  case State.SUCCESS: console.log(query.data); break;
  case State.ERROR: console.error(query.error); break;
}
```

</div>

---

> [Easy To Use And Hard To Misuse](http://principles-wiki.net/principles:easy_to_use_and_hard_to_misuse)

---
layout: default
transition: view-transition
---

<div view-transition-name="type">

```rust
enum Query {
    IsLoading,
    IsSuccess(String),
    HasError(Error),
}
```

</div>

<div view-transition-name="query">

```rust
let query: Query = http::get("https://example.com");
```

</div>

<div view-transition-name="pattern-matching">

```rust
match query { // <- pattern matching AKA switch plus puissant
    Query::IsLoading => println!("is loading"),
    Query::IsSuccess(data) => println!("{}", data),
    Query::HasError(error) => eprintln!("{}", error),
};
```

</div>

---
layout: default
transition: view-transition
---

<div view-transition-name="type">

```rust
enum Query {
    IsLoading,
    Loaded(Response),
}
enum Response {
    IsSuccess(String),
    HasError(Error),
}
```

</div>

<div view-transition-name="query">

```rust
let query: Query = http::get("https://example.com");
```

</div>

<div view-transition-name="pattern-matching">

```rust
match query {
    Query::IsLoading => println!("is loading"),
    Query::Loaded(Response::IsSuccess(data)) => println!("{}", data),
    Query::Loaded(Response::HasError(error)) => eprintln!("{}", error),
};
```

</div>

---

```rust
type Query = Option<Result<String, Error>>;
```

---

Compilateur force vérifier tout les cas

<v-click>

dont les cas alternatifs

</v-click>

---

Pas d'exception

<v-click>

`panic!()`

</v-click>

---

Verbeux

---

Difficile à faire compiler au début

---

Quand ça compile = ça fonctionne

😌

---
layout: section
---

## [Typage](https://en.wikipedia.org/wiki/Comparison_of_programming_languages_by_type_system)

---
class: ""
---

<table>
    <thead>
        <tr>
            <th>Rust <vscode-icons:file-type-rust /></th>
            <th>JavaScript <devicon:javascript /></th>
            <th>TypeScript <devicon:typescript /></th>
            <th>Python <devicon:python /></th>
        </tr>
    </thead>
    <tbody>
        <tr v-click="1">
            <td>Fort</td>
            <td>Faible</td>
            <td>Fort ?</td>
            <td>Fort</td>
        </tr>
        <tr v-click="2">
            <td>Static</td>
            <td>Dynamic</td>
            <td>Static</td>
            <td>Dynamic</td>
        </tr>
        <tr v-click="3">
            <td>Inferred <code>*</code></td>
            <td></td>
            <td>Inferred</td>
            <td></td>
        </tr>
        <tr v-click="5">
            <td>Nominal</td>
            <td>Duck 🦆</td>
            <td>Structural / Duck 🦆</td>
            <td>Duck 🦆</td>
        </tr>
    </tbody>
</table>

<div style="position: absolute;">
<VSwitch>
<template #4-5>

`*` sauf entrée / sortie des fonctions

</template>
<template #6>

> If it walks like a duck and it quacks like a duck, then it must be a duck

</template>
</VSwitch>
</div>

<br />

---
layout: section
---

## Tooling

---
layout: default
zoom: 0.7
---

<table style="text-align: left;">
    <thead>
        <tr>
            <th>Langage</th>
            <th>TypeScript <devicon:typescript /></th>
            <th>Rust <vscode-icons:file-type-rust /></th>
            <th>Python <devicon:python /></th>
        </tr>
    </thead>
    <tbody>
        <tr v-click>
            <th>Version Manager</th>
            <td>FNM, NVM, n…</td>
            <td><a href="https://rustup.rs">Rustup</a></td>
            <td>🤷 pyenv ?</td>
        </tr>
        <tr v-click>
            <th>Plateforme</th>
            <td>NodeJS <devicon:nodejs />, Deno <vscode-icons:file-type-deno />,
            bun <devicon:bun /></td>
            <td>🚫 binaire</td>
            <td>CPython <vscode-icons:file-type-cython />, PyPy…</td>
        </tr>
        <tr v-click>
            <th>Compiler</th>
            <td><code>tsc</code></td>
            <td>Cargo <vscode-icons:file-type-cargo /> (via <code>rustc</code>)</td>
            <td>🚫 interprété</td>
        </tr>
        <tr v-click>
            <th>Dependencies Manager</th>
            <td>Yarn <devicon:yarn />, NPM <devicon:npm-wordmark />…</td>
            <td>Cargo <vscode-icons:file-type-cargo /></td>
            <td>Poetry <devicon:poetry /></td>
        </tr>
        <tr v-click>
            <th>Formater</th>
            <td>Prettier <vscode-icons:file-type-prettier /></td>
            <td>Cargo <vscode-icons:file-type-cargo /> (via <code>rustfmt</code>)</td>
            <td>Black, isort</td>
        </tr>
        <tr v-click>
            <th>Linter</th>
            <td>ESLint <devicon:eslint /></td>
            <td>Cargo <vscode-icons:file-type-cargo />, Clippy</td>
            <td>Flake8, mypy</td>
        </tr>
        <tr v-click>
            <th>Test Framework</th>
            <td>Jest, Vitest <devicon:vitest />…</td>
            <td>✅ dans le langage</td>
            <td>pytest <devicon:pytest /></td>
        </tr>
    </tbody>
</table>

<!--
Meilleur tooling que j'ai vu

Regarder votre colonne

Jython, IronPython
-->

---

Compilateur <span v-click="1">aidant 🤖❤️🤗</span>

> Il y a une erreur ici,
> <br />
> <span v-click="1">pour corriger,</span>
> <br />
> <span v-click="1">tu peux essayer de faire ça</span>

---

Test unitaire collocaliser dans les fichiers sources

---

https://github.com/rust-unofficial/awesome-rust#readme

https://roogle.hkmatsumoto.com/

https://programming-idioms.org/

https://rosettacode.org/wiki/Category:Rust

---
layout: section
---

## Fibonacci

https://the-algorithms.com/fr/algorithm/fibonacci-numbers?lang=rust

https://rosettacode.org/wiki/Fibonacci_sequence#Rust

https://programming-idioms.org/idiom/301/recursive-fibonacci-sequence

---
src: ../../pages/en/questions.html
---

---
hide: true
---

<!-- markdownlint-disable MD013 -->

| What they | Before | Transformation | After |
|:----------|:-------|:---------------|:------|
| Know      |        |                |       |
| Believe   |        |                |       |
| Feel      |        |                |       |
| Do        |        |                |       |

<!-- markdownlint-enable MD013 -->
