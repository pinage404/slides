---
favicon: 'https://pinage404.gitlab.io/favicon.ico'
addons:
  - "slidev-component-zoom"
selectable: true
canvasWidth: 700
lineNumbers: true
defaults:
  layout: center
  class: text-center
---

# Tests logiciels

---
layout: section
---

## Introduction

---
src: ../../pages/fr/disclaimer_telepathe.md
---

---

### ⚠️

<v-clicks>

* 🪛<v-click at="5">🚫➡️🛤️</v-click>
* 🔨<v-click at="5">🚫➡️🛤️</v-click>
* 🔧<v-click at="5">🚫➡️🛤️</v-click>
* 🧰<v-click at="5">🚫➡️🛤️</v-click>

</v-clicks>

<!--
langage, framework, lib ... ➡️ outils

méthode

carrière
-->

---

### ⚠️

Jouez le jeu

<!--
pendant les exercices,
vous allez faire semblant que
vous croyez que les méthodes proposées sont une bonne chose

vous déciderez ensuite si vous voulez les utiliser ou non
-->

---

* Tour de table
  * votre [niveau de connaissance](../../pages/fr/niveaux.md)
    * honnete
    * sans jugement
  * vos attentes

---
src: ../../pages/fr/niveaux.md
---

---
layout: section
---

## Pratique

---

### 🥋

[Kata](https://en.wikipedia.org/wiki/Kata#Outside_martial_arts)

[Dave Thomas 2003](https://web.archive.org/web/20040607052051/http://pragprog.com/pragdave/Practices/Kata)

<!--
un bon moyen de s'entrainer à faire des tests, c'est de faire des katas
-->

---

### 🏯

[Dojo](https://en.wikipedia.org/wiki/Dojo#Computer-related)

[Laurent Bossavit 2004](https://web.archive.org/web/20040605124346/http://bossavit.com/thoughts/)

<v-click>

[Dojo développement Paris 📜](https://github.com/dojo-developpement-paris/dojo-developpement-paris.github.io/blob/main/history.md)

</v-click>

<!--
> Si je veux apprendre le Judo, je vais m'inscrire au dojo du coin
> et y passer une heure par semaine pendant deux ans,
> au bout de quoi j'aurai peut-etre envie de pratiquer plus assidument.
>
> Des années d'entraînement supplémentaire peuvent être récompensées
> par l'obtention d'une ceinture noire,
> qui n'est que le signe d'une ascension vers une autre étape de l'apprentissage.
> Aucun maître ne cesse d'apprendre.
>
> Si je veux apprendre la programmation objet,
> mon employeur va me trouver une formation de trois jours à Java
> dans le catalogue d'une société de formation.
>
> Qu'est-ce qui ne va pas avec cette photo ?
>
> Dave Thomas a eu la bonne idée avec son Kata, mais il faut aller plus loin.
>
> Ce que je veux, c'est un dojo de programmation objet.
-->

---

### 🧪

<v-clicks>

* ❌💣💥
* ✅⚙️🧠💡

</v-clicks>

---

### 👥

---
src: ../../pages/en/strong_style.md
---

---
layout: section
---

## Théorie

---

### Pourquoi on test ?

---
layout: iframe-center
url: https://lesjoiesducode.fr/captain-america-marche-sur-mon-poste-meme-developpeur
scale: 0.5
---

---

### Pourquoi on test ?

Sérénité 😌

---

### Différent types de tests

<v-clicks depth="2">

* Manuel 👤
  * Exploratoire
  * ...
* Automatisés 🤖
  * Test Unitaire (_TU_)
  * Test d'Intégration (_TI_)
  * Test End to End (_E2E_)
  * ...

</v-clicks>

---

#### Pyramide des tests

---
layout: image
image: https://blog.octo.com/la-pyramide-des-tests-par-la-pratique-1-5/pyramide-1.webp
style: |
  background-size: contain, contain;
  background-image:
    url("https://blog.octo.com/la-pyramide-des-tests-par-la-pratique-1-5/pyramide-1.webp"),
    linear-gradient(to right bottom, hsl(0, 0%, 40%), hsl(0, 0%, 90%))
---

<small>

[_La pyramide des tests par la pratique_ par Jérôme Van Der Linden chez Octo](https://blog.octo.com/la-pyramide-des-tests-par-la-pratique-1-5/)

</small>

<!--
tendre vers cet idéal pour :
* **maximiser la confiance**
* **minimiser les couts**
* minimiser les temps d'attentes
* minimiser les temps de maintenance
-->

---

#### Test E2E

<v-click>

🤖🖱️⌨️💻🛜🖥️

</v-click>

---

#### Test d'Intégration

<v-clicks>

😀💬 🗨️🙂

😣🗯️ 🗮😕

😕 🗱 😮‍💨

</v-clicks>

---

#### Test Unitaire

<v-click>

Vérifie le comportement d'une unité fonctionnelle

</v-click>

---

### Tests automatisés 🤖

---
layout: iframe-center
url: https://www.commitstrip.com/fr/2013/10/11/mieux-et-moins-cher-que-les-tests-unitaires/
scale: 0.4
---

---
src: ../../pages/fr/xp_loop.md
---

---

#### Vérification

---

#### Avantages

<v-clicks>

* **sérénité** 😌
* **confiance**
* refactoring
* non régression
* maintenabilité
* documentation
* design
* ...

</v-clicks>

<!--
design logiciel pas UI +/- synonyme architecture
-->

---

#### Inconvénients

<v-clicks depth="2">

* apprentissage
  * ralentit
* trouver les bons tests
* contrainte méthodologique
  * ré-apprentissage de la manière de dev

</v-clicks>

<!--
vous developpiez moins rapidement lorsque vous appreniez à dev
-->

---
src: ../../pages/fr/contraintes_vs_garanties.md
---

---

Contrainte : méthodes pour écrire les tests 🤔

<v-click>

Garantie : tests de confiance 😌

</v-click>

<!--
méthode longue à apprendre
-->

---
src: ../../pages/fr/first_principles.md
layout: default
transition: view-transition
---

---
src: ../../pages/fr/tdd.md
---

---
layout: section
---

Baby steps 👶🦶

---

#### TPP

<v-click>

[Transformation Priority Premise](https://en.wikipedia.org/wiki/Transformation_Priority_Premise)

</v-click>

<v-clicks>

1. Fake
2. Triangulation
3. Généralisation

</v-clicks>

---

Axes de complexité

<!--
♟️
-->

---

Documentation as Code

<v-click>

Living documentation

[Cyrille Martraire 2019 📖](https://informit.com/livingdoc)

</v-click>

---

Cas d'exemple réel

<!--
ou au moins réalistes
-->

---

Design émergeant

<v-click>

J'ai besoin d'un moyen de locomotion :

</v-click>

<v-clicks>

* 🛹
* 🛴
* 🚲
* 🛵
* 🛺

</v-clicks>

<!--
lacher prise

[click]

[click]
j'ai besoin de pouvoir m'orienter facilement

[click]
~~je suis fainéant~~

je veux pouvoir etre assis

[click]
je ne veux pas faire d'effort

[click]
je ne veux pas conduire moi meme
-->

---

⬛ Black box

🆚

⬜ White box

---

Se déplacer sur rails 🛤️

|             |       |       |
| :---------- | :---: | :---: |
| ⬛ Black box |   🚂   |   🚆   |
| ⬜ White box |   🌫️   |   ⚡   |

---

XXième siècle

Je veux avoir une calèche qui roule à 40km/h

---

Je veux avoir **une calèche** qui roule à 40km/h

---

Je veux avoir **une calèche** qui roule à 40km/h 🐴

Je veux pouvoir **me déplacer** à 40km/h 🚗

---

Tester les détails d'implémentation

<v-clicks>

➡️ Implémentation figée ❌

Comportement ✅

</v-clicks>

---

Test Double / Doublure de test

<v-clicks>

* Fake
* Dummy
* Stub
* Spy
* Mock

</v-clicks>

<v-click>

[Martin Fowler 2006](https://martinfowler.com/bliki/TestDouble.html)

</v-click>

<!--
* [click] Fake : implem réelle mais inutilisable en prod
* [click] Dummy : remplit les arguments mais pas utilisé
* [click] Stub : fourni des data
* [click] Spy : stub qui enregistre comment on interagit avec
* [click] Mock : échoue s'il n'est pas appelé de la manière attendue
-->

---

#### Code Legacy

<v-clicks>

* écrit par qqn d'autre
* _buggué_
* "vieux"
* "crade"
* code sans test
* ...

</v-clicks>

---

#### Code Legacy

<v-clicks>

1. retro test
1. refacto
1. feature

</v-clicks>

<!--
définition legacy
-->

---
src: ../../pages/en/make_the_easy_change.md
---

---

Test Paramétré

<v-clicks>

> L'abus de tests paramétrés est dangereux pour la maintenabilité
>
> À utiliser avec modération

Une règle = un test (ou plus)

`expect` paramétré = Code Smell 👃

</v-clicks>

---
class: text-center no-ligatures
---

```python
MAX_VALUE = 3

def foo(value):
    return value <= MAX_VALUE
```

<v-clicks>

Valeurs de test minimum

* 2
* 3

Tester les limites des magics numbers

</v-clicks>

---
src: ../../pages/en/cve_openssl.md
---

---

Condition dans un test

<v-click>

Plusieurs règles vérifiées d'un coup 👃

</v-click>

---
layout: two-cols-header
---

#### Structure d'un test

::left::

<v-clicks>

1. Arrange
1. Act
1. Assert

</v-clicks>

::right::

<v-clicks>

1. Given
2. When
3. Then

</v-clicks>

---

#### SUT

<v-click>System Under Test</v-click>

---
transition: view-transition
---

Outside-in ⬇️

🆚

Inside-out ⬆️

---
transition: view-transition
---

Mockist

🆚

Classicist

---

#### ATDD

<v-clicks>

Acceptance Test Driven Development

AKA Double Loop TDD

</v-clicks>

---

#### PBT

<v-click>Property Based Testing</v-click>

---

#### Mutation Testing

---

#### Golden Master

<v-click>Snapshot testing</v-click>

---

#### TCR

<v-clicks>

`test && commit || revert`

Baby steps 👶🦶

</v-clicks>

---

##### TCRDD

<v-clicks>

🟢 green

TCR + TDD

<div>

`test && revert || commit` 🔴 red

`test && commit || revert` 🟢 green

[Xavier Detant 2018](https://blog.zenika.com/2018/12/03/tdd-est-mort-longue-vie-tcr/)

</div>

</v-clicks>

<!--
Xavier's [tcrdd](https://github.com/FaustXVI/tcrdd)

[talk](https://youtu.be/oldZGW9MH2s?si=3xSKA3ijfBToV7ur)
-->

---

IA 🤖

<v-click>

🤷😕

</v-click>

<!--
je n'ai pas testé

mes amis qui ont testé m'ont dit que
c'est un dev très junior (et un peu con) qui n'a aucune méthodologie
-->

---

✏️📼

<v-clicks>

💥☀️🌋☄️💥🌔🌋⛰️💧🦠🌲🦕🦖☄️🦣🐒🧑👋

👶 ➡️ 🧮🧑‍⚕️🎨🗿🏺📜

👴🏻 ➡️ 🗓️⏳💀

📈➿

</v-clicks>

---
layout: section
---

## Points à retenir

<v-clicks>

* Accélérer la feedback loop ➰
* FIRST Principles 1️⃣
* TDD 🔴🟢🔵
* Baby steps 👶🦶

</v-clicks>

---
src: ../../pages/en/questions.html
---

---
hide: true
---

<!-- markdownlint-disable MD013 -->

| What they | Before | Transformation | After |
|:----------|:-------|:---------------|:------|
| Know      |        |                |       |
| Believe   |        |                |       |
| Feel      |        |                |       |
| Do        |        |                |       |

<!-- markdownlint-enable MD013 -->
