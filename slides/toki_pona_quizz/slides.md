---
favicon: 'https://pinage404.gitlab.io/favicon.ico'
addons:
  - "slidev-component-zoom"
selectable: true
canvasWidth: 700
lineNumbers: true
defaults:
  layout: center
  class: text-center quizz
---

# Toki Pona Quizz

---
layout: image-left
backgroundSize: contain
image: https://i.ytimg.com/vi/gGlnvMDk2Ag/maxresdefault.jpg
---

> ona li lon ! ona li lon !

<v-click>

> It's alive ! It's alive !

</v-click>

<!-- Frankenstein 1931 -->

---
layout: image-right
backgroundSize: contain
image: https://www.dergibursa.com.tr/wp-content/uploads/2015/06/43.jpg
---

> mani mi

<v-click>

> My precious

</v-click>

<!-- Lord of the Rings: The Return of the King 2003 -->

---
layout: image-left
backgroundSize: contain
image: https://www.lucasfilm.com/app/uploads/marquee-1-34.jpg
---

> mi li mama sina

<v-click>

> Je suis ton père

</v-click>

<!-- Star Wars: Episode V - The Empire Strikes Back 1980 -->

---
layout: image-right
backgroundSize: contain
image: https://i.ytimg.com/vi/VzbDjE0xsq8/hqdefault.jpg
---

> ni li lon ala

<v-click>

> C'est pas faux

</v-click>

<!-- Kaamelott 2005-2009 -->

---
layout: image-left
backgroundSize: contain
image: https://wegotthiscovered.com/wp-content/uploads/2022/08/Ned-Stark-in-Game-of-Thrones.jpg
---

> tenpo lete li kama

<v-click>

> Winter is coming

</v-click>

<!-- Game of Thrones 2011-2019 -->

---
layout: image-right
backgroundSize: contain
image: https://www.moriareviews.com/rongulator/wp-content/uploads/Terminator-1984-12.jpg
---

> tenpo kama la mi li kama

<v-click>

> I'll be back

</v-click>

<!-- The Terminator 1984 -->

---
layout: image-left
backgroundSize: contain
image: https://3.bp.blogspot.com/_cd6_MFUGTUE/TI-qhkYbt0I/AAAAAAAAAV8/wJHhnJVi8Lo/s1600/the_cake_is_a_lie_portal.jpg
---

> moku li toki ala

<v-click>

> The cake is a lie

</v-click>

<!-- Portal 2007 -->

---
layout: image-right
backgroundSize: contain
image: https://static3.cbrimages.com/wordpress/wp-content/uploads/2019/07/iron-man-i-am-iron-man.jpg
---

> mi li jan kiwen

<v-click>

> I an Iron Man

</v-click>

<!-- Iron Man 2008 -->

---
layout: two-cols
---

<v-switch>
  <template #0><div class="the_good_place small"></div></template>
  <template #1><div class="the_good_place"></div></template>
</v-switch>

<style>
  .the_good_place {
    height: 18dvh;
    margin-inline-end: 2em;
    background-image: url(https://img.nbc.com/sites/nbcunbc/files/images/2016/9/20/NUP_173189_1661.JPG);
    background-size: contain;

    &.small {
      height: 12dvh;
      background-position-y: bottom;
      background-size: cover;
    }
  }
</style>

::right::

> kama !<br />
> ali li pona.

<v-click at="1">

> Welcome !<br />
> Everything is fine.

</v-click>

<!-- The Good Place 2016-2020 -->

---
layout: image-right
backgroundSize: contain
image: https://static1.srcdn.com/wordpress/wp-content/uploads/2020/08/Avengers-Hulk-Angry-Scene.jpg
---

> tenpo ali la mi li pilin utala

<v-click>

> I'm always angry

</v-click>

<!-- The Avengers 2012 -->

---
layout: image-left
backgroundSize: contain
image: https://static1.cbrimages.com/wordpress/wp-content/uploads/2023/04/forge-fitzwilliams-dungeons-dragons-honor-among-thieves.jpg
---

> mi li wile ala e lukin sina moli...<br />
> tan la mi li tawa

<v-click>

> Ahh, I don't want to see you die...<br />
> Which is why I'm gonna leave the room.

</v-click>

<!-- Dungeons & Dragons: Honor Among Thieves 2023 -->

---
layout: image-right
backgroundSize: contain
image: https://i.ytimg.com/vi/3xYXUeSmb-Y/maxresdefault.jpg
---

> tenpo kama la sina li kama ala !

<v-click>

> You shall not pass !

</v-click>

<!-- The Lord of the Rings: The Fellowship of the Ring 2001 -->

---
layout: image-left
backgroundSize: contain
image: https://static1.srcdn.com/wordpress/wp-content/uploads/2020/07/captain-america-civil-war-i-can-do-this-all-day.jpg
---

> mi li ken sin ni tenpo ali

<v-click>

> I can do this all day

</v-click>

<!-- Captain America: The First Avenger 2011 -->

---
layout: image-right
backgroundSize: contain
image: https://d3thpuk46eyjbu.cloudfront.net/uploads/production/7772/1549935181/original/cranston.jpg
---

> jan li kalama sinpin lupa e ni mi !

<v-click>

> I am the one who knocks!

</v-click>

<!-- Breaking Bad 2008-2013 -->

---
layout: image-left
backgroundSize: contain
image: https://i.t3hwin.com/2015/04/Benders-shiny-ass.jpg
---

> monsi mi li kiwen ala kiwen ?!

<v-click>

> Et mon cul c'est du teflon ?!

</v-click>

<!-- Futurama 1999- -->

---
layout: image-right
backgroundSize: contain
image: https://images.uesp.net/thumb/d/db/SR-npc-Whiterun_Guard.jpg/1200px-SR-npc-Whiterun_Guard.jpg
---

> tenpo pini la mi li tawa jan, sama sina.<br />
> tan mi li utala lili e insa noka...

<v-click>

> I used to be an adventurer like you.<br />
> Then i took an arrow in the knee...

</v-click>

<!-- The Elder Scrolls V: Skyrim 2011 -->

---
layout: image-left
backgroundSize: contain
image: https://media.vlipsy.com/vlips/aTNLIEWd/preview.jpg
---

> wan sike li anpa ona ali,<br />
> wan sike li alasa ona,<br />
> wan sike li kama ona ali<br />
> e insa pimeja wan ona

<v-click>

> One Ring to rule them all,<br />
> one Ring to find them,<br />
> One Ring to bring them all<br />
> and in the darkness bind them

</v-click>

<!-- The Lord of the Rings: The Fellowship of the Ring 2001 -->

---
layout: image-left
backgroundSize: contain
image: https://wallpapercave.com/wp/wp4100798.jpg
---

> toki Kote, tenpo mun la sina li wile seme ?

<!-- make linter happier -->

> sama tenpo mun, Minus, alasa anpa e ma ali !

<v-click>

> Dis Cortex, tu veux faire quoi cette nuit ?

<!-- make linter happier -->

> La même chose que chaque nuit, Minus, tenter de conquérir le MONDE !

</v-click>

<!-- Minus et Cortex 1995-1998 -->

---
layout: image-right
backgroundSize: contain
image: https://i.ytimg.com/vi/KyMbTXaGiqE/maxresdefault.jpg
---

> sina li wawa. mi.<br />
> sina li pana. mi.<br />
> taso, mi li sona sina ala e toki ala.<br />
> tan la sina li ike ni

<v-click>

> You're strong. Me.<br />
> You're generous. Me.<br />
> But I never taught you to lie.<br />
> That's why you're so bad at it !

</v-click>

<!-- The Avengers 2012 -->

---
src: ../../pages/en/questions.html
---

---
hide: true
---

<!-- markdownlint-disable MD013 -->

| What they | Before | Transformation | After |
|:----------|:-------|:---------------|:------|
| Know      |        |                |       |
| Believe   |        |                |       |
| Feel      |        |                |       |
| Do        |        |                |       |

<!-- markdownlint-enable MD013 -->
