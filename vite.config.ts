import MarkdownItAttribution from 'markdown-it-attribution'
import MarkdownItKbd from 'markdown-it-kbd'
import MarkdownItSup from 'markdown-it-sup'
import { defineConfig } from 'vite'

export default defineConfig({
    slidev: {
        markdown: {
            markdownItSetup(md) {
                md.use(MarkdownItAttribution, {
                    marker: '--',
                })
                md.use(MarkdownItKbd)
                md.use(MarkdownItSup)
            },
        },
    },
})
